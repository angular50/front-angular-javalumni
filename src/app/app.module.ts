import { FormulaireInscriptionEntrepriseComponent } from './pages/inscription/formulaire-inscription-entreprise/formulaire-inscription-entreprise.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgxPaginationModule} from 'ngx-pagination';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { NgxEditorModule } from 'ngx-editor';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { CreationProjetComponent } from './components/creation-projet/creation-projet.component';
import { HeaderAdminComponent } from './components/header-admin/header-admin.component';
import { ListeProjetsComponent } from './pages/projet/liste-projets/liste-projets.component';
import { NouveauProjetComponent } from './pages/projet/nouveau-projet/nouveau-projet.component';
import { HttpClientModule } from '@angular/common/http';
import { ProjetHttpService } from './services/projet/projet-http.service';
import { DetailProjetComponent } from './pages/projet/detail-projet/detail-projet.component';
import { FormProjetComponent } from './components/form-projet/form-projet.component';
import { FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginComponent } from './pages/login/login/login.component';
import { ListeArticlesComponent } from './pages/article/liste-articles/liste-articles.component';
import { ListeEvenementsComponent } from './pages/evenement/liste-evenements/liste-evenements.component';
import { ListeOffresComponent } from './pages/offre/liste-offres/liste-offres.component';
import { DetailArticleComponent } from './pages/article/detail-article/detail-article.component';
import { DetailEvenementComponent } from './pages/evenement/detail-evenement/detail-evenement.component';
import { DetailOffreComponent } from './pages/offre/detail-offre/detail-offre.component';
import { ProjetComponent } from './pages/mon-compte/projet/projet.component';
import { MonProfilComponent } from './pages/mon-compte/mon-profil/mon-profil.component';
import { AdminEntrepriseComponent } from './pages/admin/admin-entreprise/admin-entreprise/admin-entreprise.component';
import { AdminHomeComponent } from './pages/admin/admin-home/admin-home/admin-home.component';
import { CreationArticleComponent } from './components/creation-article/creation-article.component';
import { CreationOffreComponent } from './components/creation-offre/creation-offre.component';
import { CreationEvenementComponent } from './components/creation-evenement/creation-evenement.component'
import { FormTestComponent } from './components/form-test/form-test.component';
import { VerifNumComponent } from './pages/inscription/verif-num/verif-num.component';
import { AdminBeneficiaireComponent } from './pages/admin/admin-beneficiaire/admin-beneficiaire/admin-beneficiaire.component';
import { FormulaireInscriptionBeneficiaireComponent } from './pages/inscription/formulaire-inscription-beneficiaire/formulaire-inscription-beneficiaire.component';
import { CreerArticleComponent } from './pages/article/creer-article/creer-article.component';
import { CreerEvenementComponent } from './pages/evenement/creer-evenement/creer-evenement.component';
import { CreerOffreComponent } from './pages/offre/creer-offre/creer-offre.component';
import { MesArticlesComponent } from './pages/article/mes-articles/mes-articles.component';
import { MesOffresComponent } from './pages/offre/mes-offres/mes-offres.component';
import { MesEvenementsComponent } from './pages/evenement/mes-evenements/mes-evenements.component';
import { AdminProjetComponent } from './pages/admin/admin-projet/admin-projet/admin-projet.component';
import { GroupByPipe } from './group-by.pipe';
import { ModifMdpComponent } from './pages/mon-compte/mon-profil/modif-mdp/modif-mdp.component';
import { ModifierOffreComponent } from './pages/offre/detail-offre/modifier-offre/modifier-offre.component';
import { ModifierEvenementComponent } from './pages/evenement/detail-evenement/modifier-evenement/modifier-evenement.component';
import { ModifArticleComponent } from './pages/article/detail-article/modif-article/modif-article.component';
import { ListeQuizsComponent } from './pages/quiz/liste-quizs/liste-quizs/liste-quizs.component';
import { DetailQuizComponent } from './pages/quiz/detail-quiz/detail-quiz.component';
import { ListeUtilisateurComponent } from './pages/annuaire/liste-utilisateurs/liste-utilisateur/liste-utilisateur.component';
import { DetailUtilisateurComponent } from './pages/annuaire/detail-utilisateur/detail-utilisateur/detail-utilisateur.component';
import { ListeFaqsComponent } from './pages/faq/liste-faqs/liste-faqs/liste-faqs.component';
import { CreationQuizzComponent } from './components/creation-quizz/creation-quizz.component';
import { MesQuizComponent } from './pages/quiz/mes-quiz/mes-quiz.component';
import { ModifQuizComponent } from './pages/quiz/detail-quiz/modif-quiz/modif-quiz.component';
import { StepperComponent } from './components/stepper/stepper/stepper.component';
import { ModifQuestionComponent } from './pages/quiz/detail-quiz/modif-question/modif-question.component';
import { PdfViewerModule, PdfViewerComponent } from 'ng2-pdf-viewer';
import { DetailCoursComponent } from './pages/cours/detail-cours/detail-cours/detail-cours.component'; // <- import PdfViewerModule

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    DetailEvenementComponent,
    HeaderAdminComponent,
    ListeProjetsComponent,
    NouveauProjetComponent,
    DetailProjetComponent,
    DetailArticleComponent,
    FormProjetComponent,
    LoginComponent,
    HomePageComponent,
    CreationProjetComponent,
    ListeArticlesComponent,
    ListeEvenementsComponent,
    ListeOffresComponent,
    DetailOffreComponent,
    ProjetComponent,
    MonProfilComponent,
    AdminBeneficiaireComponent,
    AdminEntrepriseComponent,
    AdminHomeComponent,
    CreationArticleComponent,
    CreationOffreComponent,
    CreationEvenementComponent,
    FormTestComponent,
    VerifNumComponent,
    FormulaireInscriptionBeneficiaireComponent,
    FormulaireInscriptionEntrepriseComponent,
    CreerArticleComponent,
    CreerEvenementComponent,
    CreerOffreComponent,
    MesArticlesComponent,
    MesOffresComponent,
    MesEvenementsComponent,
    AdminProjetComponent,
    GroupByPipe,
    ModifMdpComponent,
    ModifierOffreComponent,
    ModifierEvenementComponent,
    ModifArticleComponent,
    ListeQuizsComponent,
    DetailQuizComponent,
    ListeUtilisateurComponent,
    DetailUtilisateurComponent,
    ListeFaqsComponent,
    CreationQuizzComponent,
    MesQuizComponent,
    ModifQuizComponent,
    StepperComponent,
    ModifQuestionComponent,
    DetailCoursComponent


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxEditorModule,
    HttpClientModule,
    AngularEditorModule,
    NgxPaginationModule,
    FormsModule,
    PdfViewerModule // <- Add PdfViewerModule to imports

  ],
  providers: [
    ProjetHttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
