import { Commentaire } from './../../models/commentaire/commentaire';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Projet} from 'src/app/models/projet/projet';
import { Git } from 'src/app/models/git/git';

@Injectable({
  providedIn: 'root'
})
export class ProjetHttpService {
  private url: string = "http://localhost:8080/projets";
  private urlGit: string = "http://localhost:8080/gits";
  private urlEtat: string = "http://localhost:8080/projets/etats";
  private urlCommentaires: string = "http://localhost:8080/commentaires/projets";
  private urlPostCommentaire: string = "http://localhost:8080/commentaires";
  private urlVerifNomGroupe:string = "http://localhost:8080/projets/verifNomGroupe";
  private urlDernierProjet: string = "http://localhost:8080/projets/dernierProjet";

  constructor(private http: HttpClient) {}

  getProjets = () => {
    return this.http.get(this.url);
  }

  getProjetsByEtat = () => {
    return this.http.get(this.urlEtat);
  }

  getProjetsById = (id: number) => {
    return this.http.get(this.url+ `/${id}`);
  }

  getSoutiensProjetsById = (id: number) => {
    return this.http.get(this.url+ `/${id}` + `/soutiens`);
  }

  getProjetsByEtatList = (etat: string) => {
    return this.http.get(this.urlEtat + `/${etat}`);
  }

  getProjetsByEtatNb = (etat: string) => {
    return this.http.get(this.urlEtat + `/${etat}` + `/nb`);
  }

  getAllGits = (id: number) => {
    return this.http.get(this.urlGit + `/parIdProjet` + `/${id}`);
  }

  getAllCommentaires = (id: number) => {
    return this.http.get(this.urlCommentaires + `/${id}`);
  }

  saveCommentaire(c: Commentaire){
    return this.http.post(this.urlPostCommentaire, c);
  }

  saveProjet(p: Projet){
    return this.http.post(this.url,p);
  }

  saveGit(g: Git){
    return this.http.put(this.urlGit,g);
  }

  verifNomGroupe(projet: Projet){
    return this.http.post(this.urlVerifNomGroupe, projet);
  }

  getLastProjet = () => {
    return this.http.get(this.urlDernierProjet);
  }

  deleteProjet(id: number){
    return this.http.delete(this.url+ `/${id}`);
  }
  modifProjet(projet:Projet) {
    return this.http.put(this.url,projet);
  }
}
