import { TestBed } from '@angular/core/testing';

import { ProjetHttpService } from './projet-http.service';

describe('ProjetHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProjetHttpService = TestBed.get(ProjetHttpService);
    expect(service).toBeTruthy();
  });
});
