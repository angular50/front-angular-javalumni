import { TestBed } from '@angular/core/testing';

import { EvenementHttpService } from './evenement-http.service';

describe('EvenementHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EvenementHttpService = TestBed.get(EvenementHttpService);
    expect(service).toBeTruthy();
  });
});
