import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Evenement } from 'src/app/models/evenement/evenement';

@Injectable({
  providedIn: 'root'
})
export class EvenementHttpService {
  private url:string = "http://localhost:8080/evenements";
  private urlVille:string = "http://localhost:8080/evenements/villes";
  private urlIdUtilisateur:string = "http://localhost:8080/evenements/users";

  constructor(private http: HttpClient) { }

  getEvenements = () => {
    return this.http.get(this.url);
  }

  getEvenementsByIdUtilisateur = (id: number) => {
    return this.http.get(this.urlIdUtilisateur+ `/${id}`);
  }

  getEvenementsById = (id: number) => {
    return this.http.get(this.url+ `/${id}`);
  }

  getEvenementsByVille = () => {
    return this.http.get(this.urlVille);
  }

  getEvenementsByVilleList = (ville: string) => {
    return this.http.get(this.urlVille + `/${ville}`);
  }

  getEvenementsByVilleNb = (ville: string) => {
    return this.http.get(this.urlVille + `/${ville}` + `/nb`);
  }

  saveEvenement(e:Evenement){
    return this.http.post(this.url,e);
  }

  deleteEvenement(id: number){
    return this.http.delete(this.url+ `/${id}`);
  }
}

