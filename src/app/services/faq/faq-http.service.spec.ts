import { TestBed } from '@angular/core/testing';

import { FaqHttpService } from './faq-http.service';

describe('FaqHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FaqHttpService = TestBed.get(FaqHttpService);
    expect(service).toBeTruthy();
  });
});
