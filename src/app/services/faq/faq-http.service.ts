import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Faq } from 'src/app/models/faq/faq';
import { ReponseFAQ } from 'src/app/models/reponseFAQ/reponse-faq';

@Injectable({
  providedIn: 'root'
})
export class FaqHttpService {
  private url:string = "http://localhost:8080/faqs";
  private urlRep:string = "http://localhost:8080/reponsesfaqs/faq";
  private urlThematique:string = "http://localhost:8080/faqs/thematiques";
  private urlReponse:string = "http://localhost:8080/reponsesfaqs";
  private urlnbFaq:string = "http://localhost:8080/reponsesfaqs/numFaq";

  constructor(private http:HttpClient) { }

  getFaqs = () => {
    return this.http.get(this.url);
  }

  saveFaq = (f: Faq) => {
    return this.http.post(this.url,f);
  }

  deleteFaq(id: number){
    return this.http.delete(this.url+ `/${id}`);
  }


  saveRepFaq = (r: ReponseFAQ) => {
    return this.http.post(this.urlReponse,r);
  }

  getRepFaqsById = (id:number) => {
    return this.http.get(this.url+ `/${id}`);
  }

  getFaqsByThematique = () => {
    return this.http.get(this.urlThematique);
  }

  getFaqsByThematiqueNb = (thematique: string) => {
    return this.http.get(this.url + `/${thematique}` + `/nb`);
  }

  getIdRepByIdFaq = (idfaq:number) => {
    return this.http.get(this.urlReponse+ `/nb`+ `/${idfaq}`);
  }

  getRepByIdFaqAndIdrep = (idfaq:number , idrep:number) => {
    return this.http.get(this.urlRep+ `/${idfaq}`+ `/${idrep}`);
  }

  getNbRepByIdFaq = () => {
    return this.http.get(this.url+ `/nb`);
  }

  getNbRep = (id: number) => {
    return this.http.get(this.urlnbFaq + `/${id}` + `/nb` );
  }
}
