import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class HomePageService {
  private urlArticle:string = "http://localhost:8080/articles/dernierArticle";
  private urlEvenement:string = "http://localhost:8080/evenements/dernierEvenement";
  private urlOffre:string = "http://localhost:8080/offres/derniereOffre";
  private urlQuiz:string = "http://localhost:8080/quizs/dernierQuiz";
  private urlFaq:string = "http://localhost:8080/faqs/dernierFaq";
  constructor(private http:HttpClient) { }

  getLastArticle = () => {
    return this.http.get(this.urlArticle);
  }

  getLastEvenement = () => {
    return this.http.get(this.urlEvenement);
  }

  getLastOffre = () => {
    return this.http.get(this.urlOffre);
  }
}
