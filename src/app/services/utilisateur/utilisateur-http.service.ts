import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Projet } from 'src/app/models/projet/projet';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { Beneficiaire } from 'src/app/models/beneficiaire/beneficiaire';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurHttpService {
  private url:string = "http://localhost:8080/utilisateurs";
  private urlBeneficiaire:string = "http://localhost:8080/utilisateurs/beneficiaires";
  private urlEntreprise:string = "http://localhost:8080/utilisateurs/entreprises";
  private urlnbProjet:string = "http://localhost:8080/utilisateurs/numProjet";
  private projet:Projet[];
  projetSubject = new Subject <Projet[]>();



  constructor(private http: HttpClient) { }

  getUtilisateurs = () => {
    return this.http.get(this.url);
  }

  orderByTypeBenefOld = () => {
    return this.http.get(this.url + `/old`);
  }

  orderByTypeBenefNew = () => {
    return this.http.get(this.url + `/new`);
  }

  getUtilisateursById = (id: string) => {
    return this.http.get(this.url+ `/${id}`);
  }

  getUtilisateurById = (id: number) => {
    return this.http.get(this.url+ `/${id}`);
  }

  getUtilisateursByNumPE = (id: string) => {
    return this.http.get(this.url + `/benef` + `/${id}`);
  }

  getUtilisateursByNumRCS = (id: string) => {
    return this.http.get(this.url + `/entre` + `/${id}`);
  }

  getNbProjets = (id: number) => {
    return this.http.get(this.urlnbProjet + `/${id}` + `/nb` );
  }

  modifProjetSoutenu = (u:Utilisateur) => {
    return this.http.put(this.url,u);
  }

  getBeneficiaires = () => {
    return this.http.get(this.urlBeneficiaire);
  }
  getEntreprises = () => {
    return this.http.get(this.urlEntreprise);
  }

  getBeneficiairesByFormation = (formation: string) => {
    return this.http.get(this.urlBeneficiaire+ `/${formation}`);
  }

  getBeneficiairesByPromotion = (promotion: string) => {
    return this.http.get(this.urlBeneficiaire+ `/${promotion}`);
  }
  getEmail = () => {
    return this.http.get(this.url);
  }
  saveBeneficiaire(b:Utilisateur){
    return this.http.post(this.url,b);
  }
  saveEntreprise(e:Utilisateur){
    return this.http.post(this.url,e);
  }

  modifUtilisateur(u:Utilisateur) {
    return this.http.put(this.url,u);
  }

  deleteUtilisateur(id: number){
    return this.http.delete(this.url+ `/${id}`);
  }
}
