import { TestBed } from '@angular/core/testing';

import { OffreHttpService } from './offre-http.service';

describe('OffreHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OffreHttpService = TestBed.get(OffreHttpService);
    expect(service).toBeTruthy();
  });
});
