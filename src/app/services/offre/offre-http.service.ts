import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Offre } from 'src/app/models/offre/offre';

@Injectable({
  providedIn: 'root'
})
export class OffreHttpService {
  private url:string = "http://localhost:8080/offres";
  private urlTypeContrat:string = "http://localhost:8080/offres/typeContrat";
  private urlIdUtilisateur:string = "http://localhost:8080/offres/users";

  constructor(private http: HttpClient) { }

  getOffres = () => {
    return this.http.get(this.url);
  }

  getOffresById = (id: number) => {
    return this.http.get(this.url+ `/${id}`);
  }

  getOffresByIdUtilisateur = (id: number) => {
    return this.http.get(this.urlIdUtilisateur+ `/${id}`);
  }

  getOffresByTypeContrat = () => {
    return this.http.get(this.urlTypeContrat);
  }

  getOffresByTypeContratList = (type: string) => {
    return this.http.get(this.urlTypeContrat + `/${type}`);
  }

  getOffresByTypeContratNb = (type: string) => {
    return this.http.get(this.urlTypeContrat + `/${type}` + `/nb`);
  }
  saveOffre(o:Offre){
    return this.http.post(this.url,o);
  }

  deleteOffre(id: number){
    return this.http.delete(this.url+ `/${id}`);
  }

}
