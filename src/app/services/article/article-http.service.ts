import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article } from 'src/app/models/article/article';

@Injectable({
  providedIn: 'root'
})
export class ArticleHttpService {
  private url:string = "http://localhost:8080/articles";
  private urlCategorie:string = "http://localhost:8080/articles/categories";
  private urlIdUtilisateur:string = "http://localhost:8080/articles/users";

  constructor(private http: HttpClient) { }

  getArticles = () => {
    return this.http.get(this.url);
  }

  getArticlesById = (id: number) => {
    return this.http.get(this.url+ `/${id}`);
  }

  getArticlesByIdUtilisateur = (id: number) => {
    return this.http.get(this.urlIdUtilisateur+ `/${id}`);
  }

  getArticlesByCat = () => {
    return this.http.get(this.urlCategorie);
  }

  getArticlesByCatList = (cat: string) => {
    return this.http.get(this.urlCategorie + `/${cat}`);
  }

  getArticlesByCatNb = (cat: string) => {
    return this.http.get(this.urlCategorie + `/${cat}` + `/nb`);
  }

  saveArticle(a:Article){
    return this.http.post(this.url,a);
  }

  deleteArticle(id: number){
    return this.http.delete(this.url+ `/${id}`);
  }
}
