import { TestBed } from '@angular/core/testing';

import { ArticleHttpService } from './article-http.service';

describe('ArticleHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArticleHttpService = TestBed.get(ArticleHttpService);
    expect(service).toBeTruthy();
  });
});
