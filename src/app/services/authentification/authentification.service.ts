import { Injectable } from '@angular/core';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
  private url:string = "http://localhost:8080/utilisateurs/login";
  private urlVerifNumPE:string = "http://localhost:8080/utilisateurs/verifNumPE";
  private urlVerifNumRCS:string = "http://localhost:8080/utilisateurs/verifNumRCS";
  constructor(private http:HttpClient) { }

  authentification(utilisateur:Utilisateur){
    return this.http.post(this.url, utilisateur);
  }

  verifNumPE(utilisateur:Utilisateur){
    return this.http.post(this.urlVerifNumPE, utilisateur);
  }

  verifNumRCS(utilisateur:Utilisateur){
    return this.http.post(this.urlVerifNumRCS, utilisateur);
  }

  login(utilisateur:Utilisateur){
    if (utilisateur.email == "melkhalfaoui@gmail.com" || "delmazure.aymeric@gmail.com" || "r.fournier70@gmail.com" && utilisateur.password == "password") {
      localStorage.setItem('email', utilisateur.email);
      return true;
    } else {
      return false;
    }
  }
}
