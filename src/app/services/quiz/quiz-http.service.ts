import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Quiz } from 'src/app/models/quiz/quiz';
import { Question } from 'src/app/models/question/question';
import { Reponse } from 'src/app/models/reponse/reponse';

@Injectable({
  providedIn: 'root'
})
export class QuizHttpService {
  private urlQstnb:string = "http://localhost:8080/questions/nb";
  private urlQuiz: string = "http://localhost:8080/quizs";
  private urlDernierQuiz: string = "http://localhost:8080/quizs/dernierQuiz";
  private urlQst:string = "http://localhost:8080/questions/quiz";
  private urlDernierQuestion:string = "http://localhost:8080/questions/dernierQuestion";
  private urlRep:string = "http://localhost:8080/reponses/question";
  private url:string = "http://localhost:8080/reponses";
  private urlCatQuiz: string = "http://localhost:8080/quizs/categories"
  private urlPostQuestion:string = "http://localhost:8080/questions";
  private urlPostReponse:string = "http://localhost:8080/reponses";
  private urlIdUtilisateur:string = "http://localhost:8080/quizs/users";

  constructor(private http:HttpClient) { }

  getQuizs = () => {
    return this.http.get(this.urlQuiz);
  }

  getQuizsByCat = () => {
    return this.http.get(this.urlCatQuiz);
  }

  getQuizsByCatNb = (categorie: string) => {
    return this.http.get(this.urlCatQuiz + `/${categorie}` + `/nb`);
  }

  getQuizsByCatList = (categorie: string) => {
    return this.http.get(this.urlCatQuiz + `/${categorie}`);
  }

  getNbQstByIdQuiz = (id: number) => {
    return this.http.get(this.urlQstnb+ `/${id}`);
  }


  getQuesionsByIdquiz = (id: number) => {
    return this.http.get(this.urlQst+ `/${id}`);
  }

  getReponsesByIdquestion = (id: number) => {
    return this.http.get(this.urlRep+ `/${id}`);
  }

  getRepByIdqstAndIdrep = (idqst: number, idrep: number) => {
    return this.http.get(this.urlRep+ `/${idqst}`+ `/${idrep}`);
  }

  getNbRepByIdQst = (idqst: number) => {
    return this.http.get(this.urlRep+ `/${idqst}`+ `/nb`);
  }

  getAllReponses = () => {
    return this.http.get(this.url);
  }

  getNbReponses = () => {
    return this.http.get(this.url+ `/listeQst`);
  }


  getIdRepByIdQuest = (idquestion: number) => {
    return this.http.get(this.url+ `/nb`+ `/${idquestion}`);
  }
  saveQuizz(q:Quiz){
    return this.http.post(this.urlQuiz,q);
  }

  saveOrUpdateQuizz(q:Quiz){
      return this.http.put(this.urlQuiz,q);
  }

  saveQuestion(q:Question){
    return this.http.post(this.urlPostQuestion,q);
  }
  saveReponse(r:Reponse){
    return this.http.post(this.urlPostReponse,r);
  }
  getLastQuizz(){
    return this.http.get(this.urlDernierQuiz);
  }
  getLastQuestion(){
    return this.http.get(this.urlDernierQuestion);
  }
  getQuizByIdUtilisateur = (id: number) => {
    return this.http.get(this.urlIdUtilisateur+ `/${id}`);
  }
  getQuizByIdQuiz = (id:number) => {
    return this.http.get(this.urlQuiz+ `/${id}`);

  }
  getQuestionById (id:number){
    return this.http.get(this.urlPostQuestion+ `/${id}`);
  }
  saveOrUpdateQuestion(q:Question){
    return this.http.put(this.urlPostQuestion,q);
  }
}
