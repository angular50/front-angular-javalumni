import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Utilisateur } from '../models/utilisateur/utilisateur';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate{
  constructor(private router:Router) {}

  utilisateurs: Utilisateur[];
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(localStorage.getItem('email') == "melkhalfaoui@gmail.com" || "delmazure.aymeric@gmail.com" || "r.fournier70@gmail.com" || "toto@gmail.com") {
        return true;
      }else {
        this.router.navigate(['/login']);
      };
  }
}
