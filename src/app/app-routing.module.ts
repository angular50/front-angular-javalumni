import { DetailCoursComponent } from './pages/cours/detail-cours/detail-cours/detail-cours.component';
import { DetailUtilisateurComponent } from './pages/annuaire/detail-utilisateur/detail-utilisateur/detail-utilisateur.component';
import { ListeUtilisateurComponent } from './pages/annuaire/liste-utilisateurs/liste-utilisateur/liste-utilisateur.component';
import { DetailQuizComponent } from './pages/quiz/detail-quiz/detail-quiz.component';
import { AdminProjetComponent } from './pages/admin/admin-projet/admin-projet/admin-projet.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreationProjetComponent } from './components/creation-projet/creation-projet.component';
import { ListeProjetsComponent } from './pages/projet/liste-projets/liste-projets.component';
import { DetailProjetComponent } from './pages/projet/detail-projet/detail-projet.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './pages/login/login/login.component';
import { ListeArticlesComponent } from './pages/article/liste-articles/liste-articles.component';
import { DetailArticleComponent } from './pages/article/detail-article/detail-article.component';
import { ListeOffresComponent } from './pages/offre/liste-offres/liste-offres.component';
import { ListeEvenementsComponent } from './pages/evenement/liste-evenements/liste-evenements.component';
import { DetailEvenementComponent } from './pages/evenement/detail-evenement/detail-evenement.component';
import { DetailOffreComponent } from './pages/offre/detail-offre/detail-offre.component';
import { MonProfilComponent } from './pages/mon-compte/mon-profil/mon-profil.component';
import { ProjetComponent } from './pages/mon-compte/projet/projet.component';
import { AdminHomeComponent } from './pages/admin/admin-home/admin-home/admin-home.component';
import { AdminBeneficiaireComponent } from './pages/admin/admin-beneficiaire/admin-beneficiaire/admin-beneficiaire.component';
import { AdminEntrepriseComponent } from './pages/admin/admin-entreprise/admin-entreprise/admin-entreprise.component';
import { FormTestComponent } from './components/form-test/form-test.component';
import { NouveauProjetComponent } from './pages/projet/nouveau-projet/nouveau-projet.component';
import { VerifNumComponent } from './pages/inscription/verif-num/verif-num.component';
import { FormulaireInscriptionBeneficiaireComponent } from './pages/inscription/formulaire-inscription-beneficiaire/formulaire-inscription-beneficiaire.component';
import { FormulaireInscriptionEntrepriseComponent } from './pages/inscription/formulaire-inscription-entreprise/formulaire-inscription-entreprise.component';
import { CreerArticleComponent } from './pages/article/creer-article/creer-article.component'
import {CreerOffreComponent} from './pages/offre/creer-offre/creer-offre.component'
import { MesArticlesComponent } from './pages/article/mes-articles/mes-articles.component';
import { MesEvenementsComponent } from './pages/evenement/mes-evenements/mes-evenements.component';
import { MesOffresComponent } from './pages/offre/mes-offres/mes-offres.component';
import { CreationEvenementComponent } from './components/creation-evenement/creation-evenement.component';
import { ModifMdpComponent } from './pages/mon-compte/mon-profil/modif-mdp/modif-mdp.component'
import { ModifierEvenementComponent } from './pages/evenement/detail-evenement/modifier-evenement/modifier-evenement.component';
import { ModifierOffreComponent } from './pages/offre/detail-offre/modifier-offre/modifier-offre.component'
import { ModifArticleComponent } from './pages/article/detail-article/modif-article/modif-article.component'
import { ListeQuizsComponent } from './pages/quiz/liste-quizs/liste-quizs/liste-quizs.component';
import { ListeFaqsComponent } from './pages/faq/liste-faqs/liste-faqs/liste-faqs.component';
import { CreationQuizzComponent } from './components/creation-quizz/creation-quizz.component';
import { MesQuizComponent } from './pages/quiz/mes-quiz/mes-quiz.component';
import { ModifQuizComponent } from './pages/quiz/detail-quiz/modif-quiz/modif-quiz.component'
import { ModifQuestionComponent } from './pages/quiz/detail-quiz/modif-question/modif-question.component'
const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'verifNum',
    component: VerifNumComponent, canActivate: [AuthGuard]
  },
  {
    path: 'pdf',
    component: DetailCoursComponent, canActivate: [AuthGuard]
  },
  {
    path: 'inscriptionBeneficiaire',
    component: FormulaireInscriptionBeneficiaireComponent, canActivate: [AuthGuard]
  },
  {
    path: 'inscriptionEntreprise',
    component: FormulaireInscriptionEntrepriseComponent, canActivate: [AuthGuard]
  },
  {
    path: 'admin',
    component: AdminHomeComponent, canActivate: [AuthGuard]
  },
  {
    path: 'admin/admin-beneficiaires',
    component: AdminBeneficiaireComponent, canActivate: [AuthGuard]
  },
  {
    path: 'admin/admin-projet',
    component: AdminProjetComponent, canActivate: [AuthGuard]
  },
  {
    path: 'admin/admin-entreprises',
    component: AdminEntrepriseComponent, canActivate: [AuthGuard]
  },
  {
    path: 'projets/liste-projets',
    component: ListeProjetsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'projets/detail-projet/:id',
    component: DetailProjetComponent, canActivate: [AuthGuard]
  },
  {
    path: 'quizs/liste-quizs',
    component: ListeQuizsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'quizs/creation-quizs',
    component: CreationQuizzComponent, canActivate: [AuthGuard]
  },
  {
    path: 'quizs/detail-quiz/:id',
    component: DetailQuizComponent, canActivate: [AuthGuard]
  },
  {
    path: 'quizs/mes-quizzes/:id',
    component: MesQuizComponent, canActivate: [AuthGuard]
  },
  {
    path: 'quizs/modifier-quizs/:id',
    component: ModifQuizComponent, canActivate: [AuthGuard]
  },
  {
    path: 'quizs/modifier-question/:id',
    component: ModifQuestionComponent, canActivate: [AuthGuard]
  },
  {
  path: 'evenements/modifier-evenement/:id',
  component: ModifierEvenementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'articles/liste-articles',
    component: ListeArticlesComponent, canActivate: [AuthGuard]
  },
  {
    path: 'articles/detail-article/:id',
    component: DetailArticleComponent, canActivate: [AuthGuard]
  },
  {
    path: 'articles/modif-article/:id',
    component: ModifArticleComponent, canActivate: [AuthGuard]
  },
  {
    path: 'articles/mes-articles/:id',
    component: MesArticlesComponent, canActivate: [AuthGuard]
  },
  {
    path: 'evenements/mes-evenements/:id',
    component: MesEvenementsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'offres/mes-offres/:id',
    component: MesOffresComponent, canActivate: [AuthGuard]
  },
  {
    path: 'offres/liste-offres',
    component: ListeOffresComponent, canActivate: [AuthGuard]
  },
  {
    path: 'offres/detail-offre/:id',
    component: DetailOffreComponent, canActivate: [AuthGuard]
  },
  {
    path: 'offres/modifier-offre/:id',
    component: ModifierOffreComponent, canActivate: [AuthGuard]
  },
  {
    path: 'evenements/liste-evenements',
    component: ListeEvenementsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'evenements/detail-evenement/:id',
    component: DetailEvenementComponent, canActivate: [AuthGuard]
  },
  {
    path: 'mon-compte/mon-profil',
    component: MonProfilComponent, canActivate: [AuthGuard]
  },
  {
    path: 'mon-compte/projet',
    component: ProjetComponent, canActivate: [AuthGuard]
  },
  {
    path: 'mon-compte/mon-profil/:id',
    component: ModifMdpComponent, canActivate: [AuthGuard]
  },
  {
    path: 'homePage',
    component: HomePageComponent, canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomePageComponent
  },
  {
    path: 'creation-projet',
    component: CreationProjetComponent
  },
  {
path: 'form-test',
component: FormTestComponent
  },
  {
    path: 'nouveau-projet',
    component: NouveauProjetComponent
      },
   {
     path: 'articles/creer-article',
    component: CreerArticleComponent
   },
   {
    path: 'offres/creer-offre',
   component: CreerOffreComponent
  },
  {
    path: 'evenements/creer-evenement',
   component: CreationEvenementComponent
  },
  {
    path: 'utilisateurs/liste-utilisateurs',
    component: ListeUtilisateurComponent, canActivate: [AuthGuard]
  },
  {
    path: 'utilisateurs/detail-utilisateur/:id',
    component: DetailUtilisateurComponent, canActivate: [AuthGuard]
  },
  {
    path: 'faqs/liste-faqs',
    component: ListeFaqsComponent, canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {

}