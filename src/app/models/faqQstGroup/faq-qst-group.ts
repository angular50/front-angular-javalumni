export class FaqQstGroup {
    constructor (
        public idfaq: number,
        public thematique: string,
        public question: string,
        public count:number) {
    }
}
