import { Quiz } from './../quiz/quiz';
export class Question {
    idquestion: number;
    categorie: string;
    corrige: string;
    difficulte: string;
    question: string;
    quiz: Quiz = new Quiz();
}
