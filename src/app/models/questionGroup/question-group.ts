export class QuestionGroup {
    constructor (
        public numqst:number,
        public rep1:string,
        public rep2:string,
        public rep3:string
        ) {}
}
