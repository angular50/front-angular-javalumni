import { Utilisateur } from '../utilisateur/utilisateur';

export class Faq {
  idfaq: number;
  thematique: string;
  question: string;
  utilisateur: Utilisateur = new Utilisateur();
}
