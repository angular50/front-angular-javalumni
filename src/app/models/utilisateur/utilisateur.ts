import { Projet } from '../projet/projet';
import { Evenement } from '../evenement/evenement';

export class Utilisateur {
    idutilisateur: number;
    numPE: number;
    formation: string;
    promotion: string;
    numRCS: number;
    nom: string;
    prenom: string;
    email: string;
    password: string;
    nomEntreprise: string;
    ville: string;
    presentation: string;
    dateDebut: Date;
    dateFin: Date;
    linkedin: string;
    siteWeb: string;
    projetSoutenu: number;
    projet : Projet = new Projet();
}
