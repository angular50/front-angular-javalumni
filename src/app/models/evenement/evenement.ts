import { Utilisateur } from '../utilisateur/utilisateur';

export class Evenement {
    idevenement: number;
    nom: string;
    description: string;
    localisation: string;
    dateEvenement: Date;
    datePublication:Date;
    utilisateur : Utilisateur = new Utilisateur();
}
