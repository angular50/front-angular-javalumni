import { Question } from '../question/question';

export class Reponse {
    idreponse: number;
    // tslint:disable-next-line: variable-name
    bonneReponse: number;
    reponse: string;
    question : Question = new Question();
}
