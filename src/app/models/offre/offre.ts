import { Utilisateur } from '../utilisateur/utilisateur';

export class Offre {
    idoffre: number;
    intitule: string;
    localisation: string;
    description: string;
    typeContrat: string;
    datePublication: Date;
    utilisateur : Utilisateur = new Utilisateur();
}
