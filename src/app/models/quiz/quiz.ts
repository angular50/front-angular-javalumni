import { Utilisateur } from './../utilisateur/utilisateur';
export class Quiz {
    idquiz: number;
    categorie: string;
    descriptif: string;
    dateParution: Date;
    difficulte: string;
    idutilisateur: number;
    nom: string;
    utilisateur : Utilisateur = new Utilisateur();
}
