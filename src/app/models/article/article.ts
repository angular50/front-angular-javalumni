import { Utilisateur } from '../utilisateur/utilisateur';

export class Article {
    idarticle: number;
    categorie: string;
    nom: string;
    contenu: string;
    dateParution: Date;
    utilisateur : Utilisateur = new Utilisateur();
      
}
