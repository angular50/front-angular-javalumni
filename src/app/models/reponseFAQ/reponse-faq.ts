import { Faq } from '../faq/faq';

export class ReponseFAQ {
    idreponsefaq: number;
    // tslint:disable-next-line: variable-name
    reponse: string;
    faq: Faq = new Faq();
}
