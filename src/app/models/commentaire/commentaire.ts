import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
export class Commentaire {
  idcommentaire: number;
  idprojet: number;
  text: string;
  utilisateur: Utilisateur = new Utilisateur();
}
