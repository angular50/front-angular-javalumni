export class ProjetGroup {
    constructor (
      public idprojet:number,
      public titre:string,
      public description:string,
      public fonctionnalites:string,
      public technologies:string,
      public groupe:string,
      public etat:string,
      public count:number) {
    }
}
