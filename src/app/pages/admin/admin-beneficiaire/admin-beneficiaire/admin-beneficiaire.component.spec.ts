import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminBeneficiaireComponent } from './admin-beneficiaire.component';

describe('AdminBeneficiaireComponent', () => {
  let component: AdminBeneficiaireComponent;
  let fixture: ComponentFixture<AdminBeneficiaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminBeneficiaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminBeneficiaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
