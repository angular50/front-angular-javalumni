import { Component, OnInit } from '@angular/core';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { FormBuilder, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-admin-beneficiaire',
  templateUrl: './admin-beneficiaire.component.html',
  styleUrls: ['./admin-beneficiaire.component.scss']
})
export class AdminBeneficiaireComponent implements OnInit {
  formsubmitted= true;
  utilisateurs:Utilisateur[];
  adminBenef : FormGroup;
  idOK: number;
  p: number = 1;
  promotions=['PROMOTION', 'ID62', 'ID63', 'ID55'];
  formations=['FORMATION', 'JAVA', 'FULLSTACK', 'DOTNET'];

  constructor(private utilisateurService: UtilisateurHttpService,
    private fb: FormBuilder ) {
      this.adminBenef = fb.group({
        numPE: "",
        promotion: "",
        formation: "",
        dateDebut: "",
        dateFin: ""

      })
  }

  ngOnInit() {

    this.utilisateurService.getBeneficiaires().subscribe(
      (utilisateurs:Utilisateur[])=> {
        this.utilisateurs=utilisateurs;
        console.log(utilisateurs);},
      err => {console.warn(err);}
    )
  }
    ajoutBeneficiaire=()=>{
      this.formsubmitted = false;
      console.log(this.adminBenef.value);
      let utilisateurs = new Utilisateur;
      utilisateurs.numPE= this.adminBenef.value.numPE;
      utilisateurs.promotion = this.adminBenef.value.promotion;
      utilisateurs.formation = this.adminBenef.value.formation;
      utilisateurs.dateDebut = this.adminBenef.value.dateDebut;
      utilisateurs.dateFin = this.adminBenef.value.dateFin;
      utilisateurs.projet.idprojet = 1;

      this.utilisateurService.saveBeneficiaire(utilisateurs).subscribe(
        ()=>{},
        err => {console.warn(err);});

        this.formsubmitted = true

  }

  deleteUtilisateur(id){
    var r = confirm("Confirmer la suppression");
    if (r == true) {
      this.idOK = id;
    } else {
      this.idOK = 99;
    }
    this.utilisateurService.deleteUtilisateur(this.idOK).subscribe(
      ()=> {},
      err => {console.warn(err);});
  }
}

