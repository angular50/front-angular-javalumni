import { Component, OnInit } from '@angular/core';
import { ArticleHttpService } from 'src/app/services/article/article-http.service';
import { EvenementHttpService } from 'src/app/services/evenement/evenement-http.service';
import { OffreHttpService } from 'src/app/services/offre/offre-http.service';
import { Offre } from 'src/app/models/offre/offre';
import { Article } from 'src/app/models/article/article';
import { Evenement } from 'src/app/models/evenement/evenement';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss']
})
export class AdminHomeComponent implements OnInit {
  offres:Offre[];
  articles:Article[];
  evenements:Evenement[];
  offre: FormGroup;
  idOK: number;
  constructor(
    private articleService: ArticleHttpService,
    private evenementService: EvenementHttpService,
    private offreService: OffreHttpService,
    private fb: FormBuilder) {
      this.offre = fb.group({})
    }

  ngOnInit() {

    this.offreService.getOffres().subscribe(
      (offres:Offre[])=> {
        this.offres=offres;
        console.log(offres);},
      err => {console.warn(err);}
    )

    this.articleService.getArticles().subscribe(
      (articles:Article[])=> {
        this.articles=articles;
        console.log(articles);},
      err => {console.warn(err);}
    )

    this.evenementService.getEvenements().subscribe(
      (evenements:Evenement[])=> {
        this.evenements=evenements;
        console.log(evenements);},
      err => {console.warn(err);}
    )

      }

      deleteOffre(id){
        var r = confirm("Confirmer la suppression");
        if (r == true) {
          this.idOK = id;
        } else {
          this.idOK = 99;
        }

        this.offreService.deleteOffre(this.idOK).subscribe(
          ()=> {},
          err => {console.warn(err);});
      }

      deleteArticle(id){
        this.articleService.deleteArticle(id).subscribe(
          ()=> {},
          err => {console.warn(err);});
      }

      deleteEvenement(id){
        this.evenementService.deleteEvenement(id).subscribe(
          ()=> {},
          err => {console.warn(err);});
      }
}
