import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { ProjetHttpService } from 'src/app/services/projet/projet-http.service';
import { Component, OnInit } from '@angular/core';
import { Projet } from 'src/app/models/projet/projet';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { ProjetGroup } from 'src/app/models/projetGroup/projet-group';
import { FaqHttpService } from 'src/app/services/faq/faq-http.service';
import { Faq } from 'src/app/models/faq/faq';
import { FaqGroup } from 'src/app/models/faqGroup/faq-group';
import { FaqQstGroup } from 'src/app/models/faqQstGroup/faq-qst-group';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ReponseFAQ } from 'src/app/models/reponseFAQ/reponse-faq';

@Component({
  selector: 'app-admin-projet',
  templateUrl: './admin-projet.component.html',
  styleUrls: ['./admin-projet.component.scss']
})
export class AdminProjetComponent implements OnInit {
  projets:Projet[];
  faqs:Faq[];
  projetsNb = new Array<ProjetGroup>();
  faqsNb = new Array<FaqQstGroup>();
  utilisateurs:Utilisateur[];
  idOK: number;
  p: number = 1;
  ajoutRepFAQ: FormGroup;
  constructor(private projetService: ProjetHttpService,
              private faqService: FaqHttpService,
              private utilisateurService: UtilisateurHttpService,
              private fb:FormBuilder){ 
this.ajoutRepFAQ = fb.group({
  reponse:"",
  idqstfaq:"",
})
}

  ngOnInit() {

    this.projetService.getProjets().subscribe(
      (projets:Projet[])=> {
        this.projets=projets;
        console.log(projets);},
      err => {console.warn(err);}
    )

    this.getProjetsGroup();
    this.getFaqsGroup();

    this.utilisateurService.getBeneficiaires().subscribe(
      (utilisateurs:Utilisateur[])=> {
        this.utilisateurs=utilisateurs;
        console.log(utilisateurs);},
      err => {console.warn(err);}
    )
  }

  getProjetsGroup() {
    this.projetService.getProjets().subscribe(
      (projets:Projet[])=> {
        projets.forEach(
          projets =>
        this.utilisateurService.getNbProjets(projets.idprojet).subscribe(
          (count: number) => {
            this.projetsNb.push(new ProjetGroup(
              projets.idprojet,
              projets.titre,
              projets.description,
              projets.fonctionnalites,
              projets.groupe,
              projets.etat,
              projets.technologies,
              count))
          },
          err => {console.warn(err);}
        )
        )
        console.log(projets);},
      err => {console.warn(err);}
    )
  }

  getFaqsGroup() {
    this.faqService.getFaqs().subscribe(
      (faqs:Faq[])=> {
        console.log(faqs);
        faqs.forEach(
          faqs =>
        this.faqService.getNbRep(faqs.idfaq).subscribe(
          (count: number) => {
            this.faqsNb.push(new FaqQstGroup(
              faqs.idfaq,
              faqs.thematique,
              faqs.question,
              count))
          },
          err => {console.warn(err);}
        )
        )
        console.log(faqs);},
      err => {console.warn(err);}
    )
  }

  deleteProjet(id){
    var r = confirm("Confirmer la suppression");
    if (r == true) {
      this.idOK = id;
    } else {
      this.idOK = 99;
    }

    this.projetService.deleteProjet(this.idOK).subscribe(
      ()=> {},
      err => {console.warn(err);});
  }

  deleteQstFaq(id){
    var r = confirm("Confirmer la suppression");
    if (r == true) {
      this.idOK = id;
    } else {
      this.idOK = 99;
    }

    this.faqService.deleteFaq(this.idOK).subscribe(
      ()=> {},
      err => {console.warn(err);});
  }

  ajoutNumFAQLocal(id:number){
    localStorage.setItem("idFaq", JSON.stringify(id));
  }

  modifFAQ=()=>{
    let reponseFAQ= new ReponseFAQ;
    reponseFAQ.reponse= this.ajoutRepFAQ.value.reponse;
    reponseFAQ.faq.idfaq= JSON.parse(localStorage.getItem("idFaq"));
   
      this.faqService.saveRepFaq(reponseFAQ).subscribe(
      ()=> {
      },
      err => {console.warn(err);});
    }

}
