import { Component, OnInit } from '@angular/core';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-admin-entreprise',
  templateUrl: './admin-entreprise.component.html',
  styleUrls: ['./admin-entreprise.component.scss']
})
export class AdminEntrepriseComponent implements OnInit {
  formsubmitted= true;
  utilisateurs:Utilisateur[];
  adminEntreprise : FormGroup;
  idOK: number;
  p: number = 1;
  constructor(private utilisateurService: UtilisateurHttpService,
    private fb: FormBuilder) { 
      this.adminEntreprise=fb.group({
        numRCS:"",
        nomEntreprise:"",
      })
    }

    ngOnInit() {

      this.utilisateurService.getEntreprises().subscribe(
        (utilisateurs:Utilisateur[])=> {
          this.utilisateurs=utilisateurs;
          console.log(utilisateurs);},
        err => {console.warn(err);}
      )
    }

    ajoutEntreprise=()=>{
      this.formsubmitted = false;
      console.log(this.adminEntreprise.value);
      let utilisateurs = new Utilisateur;
      utilisateurs.numRCS= this.adminEntreprise.value.numRCS;
      utilisateurs.nomEntreprise = this.adminEntreprise.value.nomEntreprise;
      utilisateurs.projet.idprojet = 1;
      utilisateurs.projetSoutenu = 1;

      this.utilisateurService.saveEntreprise(utilisateurs).subscribe(
        ()=>{},
        err => {console.warn(err);});

        this.formsubmitted = true
    
  }

  deleteUtilisateur(id){
    var r = confirm("Confirmer la suppression");
    if (r == true) {
      this.idOK = id;
    } else {
      this.idOK = 99;
    }
    this.utilisateurService.deleteUtilisateur(this.idOK).subscribe(
      ()=> {},
      err => {console.warn(err);});
  }
}
