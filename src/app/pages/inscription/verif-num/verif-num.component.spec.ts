import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifNumComponent } from './verif-num.component';

describe('VerifNumComponent', () => {
  let component: VerifNumComponent;
  let fixture: ComponentFixture<VerifNumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifNumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifNumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
