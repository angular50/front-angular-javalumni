import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { Router } from '@angular/router';
import { AuthentificationService } from 'src/app/services/authentification/authentification.service';

@Component({
  selector: 'app-verif-num',
  templateUrl: './verif-num.component.html',
  styleUrls: ['./verif-num.component.scss']
})
export class VerifNumComponent implements OnInit {
  content = 'mainContent';
  verifFormPE: FormGroup;
  verifFormRCS: FormGroup;
  utilisateur: Utilisateur;
  constructor(private fb: FormBuilder, private authService: AuthentificationService, private router:Router)
    {this.verifFormPE = fb.group({
    numPE: ""
 });
 this.verifFormRCS = fb.group({
  numRCS: ""
});
}
  ngOnInit() {
  }
    verifNumPE=()=>{
      let utilisateur = new Utilisateur;
      utilisateur.numPE = this.verifFormPE.value.numPE;

      this.authService.verifNumPE(utilisateur).subscribe(
        (utilisateur:Utilisateur)=> {
          this.utilisateur=utilisateur;
          localStorage.setItem("numPE", JSON.stringify(utilisateur.numPE))
          if (localStorage.length === 0) {
            this.router.navigate(['/login']);
          } else {
            this.router.navigate(['/inscriptionBeneficiaire']);
          }
        },
        err => {console.warn(err);});
      }

      verifNumRCS=()=>{
        let utilisateur = new Utilisateur;
        utilisateur.numRCS = this.verifFormRCS.value.numRCS;

        this.authService.verifNumRCS(utilisateur).subscribe(
          (utilisateur:Utilisateur)=> {
            this.utilisateur=utilisateur;
            localStorage.setItem("numRCS", JSON.stringify(utilisateur.numRCS))
            localStorage.setItem("nomEntreprise", utilisateur.nomEntreprise)
            if (localStorage.length === 0) {
              this.router.navigate(['/login']);
            } else {
              this.router.navigate(['/inscriptionEntreprise']);
            }
          },
          err => {console.warn(err);});
        }
}
