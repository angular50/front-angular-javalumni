import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaireInscriptionEntrepriseComponent } from './formulaire-inscription-entreprise.component';

describe('FormulaireInscriptionEntrepriseComponent', () => {
  let component: FormulaireInscriptionEntrepriseComponent;
  let fixture: ComponentFixture<FormulaireInscriptionEntrepriseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormulaireInscriptionEntrepriseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaireInscriptionEntrepriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
