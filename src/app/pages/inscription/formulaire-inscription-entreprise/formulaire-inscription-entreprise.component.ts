import { Component, OnInit } from '@angular/core';
import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from '../CustomValidator';
import { Router } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-formulaire-inscription-entreprise',
  templateUrl: './formulaire-inscription-entreprise.component.html',
  styleUrls: ['./formulaire-inscription-entreprise.component.scss']
})
export class FormulaireInscriptionEntrepriseComponent implements OnInit {
  utilisateur: Utilisateur;
  inscriptionEntre: FormGroup;
  villes=['Lille', 'Lyon', 'Paris', 'Roubaix', 'Tourcoing'];
  constructor(private utilisateurService: UtilisateurHttpService, private fb: FormBuilder,
    private router: Router) {
    this.inscriptionEntre = fb.group({
      numRCS: localStorage.getItem("numRCS"),
      nom:"",
      nomEntreprise:"",
      prenom: "",
      siteWeb: "",
      ville:"",
      presentation:"",
      linkedin:"",
      email: [
        null,
        Validators.compose([Validators.email, Validators.required])
      ],
      password:[
        null,
        Validators.compose([
          Validators.required,
          // check whether the entered password has a number
          CustomValidators.patternValidator(/\d/, {
            hasNumber: true
          }),
          // check whether the entered password has upper case letter
          CustomValidators.patternValidator(/[A-Z]/, {
            hasCapitalCase: true
          }),
          // check whether the entered password has a lower case letter
          CustomValidators.patternValidator(/[a-z]/, {
            hasSmallCase: true
          }),
          // check whether the entered password has a special character
          CustomValidators.patternValidator(
            /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
            {
              hasSpecialCharacters: true
            }
          ),
          Validators.minLength(8)
        ])
      ],
      confirmPassword:[null, Validators.compose([Validators.required])]
    },
    {
      // check whether our password and confirm password match
      validator: CustomValidators.passwordMatchValidator
    })
   }

  ngOnInit() {
    this.getbyId(localStorage.getItem('numRCS'));
  }

  inscriptionEntreprise=()=>{
    localStorage.setItem("nom", this.inscriptionEntre.value.nom);
    localStorage.setItem("prenom", this.inscriptionEntre.value.prenom);
    localStorage.setItem("presentation", this.inscriptionEntre.value.presentation);
    localStorage.setItem("linkedin", this.inscriptionEntre.value.linkedin);
    localStorage.setItem("ville",this.inscriptionEntre.value.ville);
    localStorage.setItem("email", this.inscriptionEntre.value.email);
    localStorage.setItem("password", this.inscriptionEntre.value.password);
    localStorage.setItem("siteWeb", this.inscriptionEntre.value.siteWeb);
    localStorage.setItem("idProjet", "1");
    localStorage.setItem("projetSoutenu", "1");

    let utilisateur = new Utilisateur;
    utilisateur.idutilisateur = JSON.parse(localStorage.getItem("idUtilisateur"));
    utilisateur.numRCS = JSON.parse(localStorage.getItem("numRCS"));
    utilisateur.promotion = localStorage.getItem("promotion");
    utilisateur.formation = localStorage.getItem("formation");
    utilisateur.nom = localStorage.getItem("nom");
    utilisateur.prenom = localStorage.getItem("prenom");
    utilisateur.presentation = localStorage.getItem("presentation");
    utilisateur.linkedin = localStorage.getItem("linkedin");
    utilisateur.ville = localStorage.getItem("ville");
    utilisateur.email = localStorage.getItem("email");
    utilisateur.password = localStorage.getItem("password");
    utilisateur.siteWeb = localStorage.getItem("siteWeb");
    utilisateur.nomEntreprise = localStorage.getItem("nomEntreprise");
    utilisateur.projet.idprojet = 1;

    this.utilisateurService.modifUtilisateur(utilisateur).subscribe(
      ()=>{
        this.router.navigate(['/login']);
      },
      err => {console.warn(err);});
}

  getbyId(id:string) {
    this.utilisateurService.getUtilisateursByNumRCS(id).subscribe(
      (utilisateur:Utilisateur)=> {
        this.utilisateur=utilisateur;
        localStorage.setItem("idUtilisateur", JSON.stringify(utilisateur.idutilisateur));
        localStorage.setItem("numRCS", JSON.stringify(utilisateur.numRCS));
        localStorage.setItem("formation", utilisateur.formation);
        localStorage.setItem("promotion", utilisateur.promotion);},
      err => {console.warn(err);}
    )
  }
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '15rem',

      maxHeight: 'auto',
      width: '53rem',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',

};
}
