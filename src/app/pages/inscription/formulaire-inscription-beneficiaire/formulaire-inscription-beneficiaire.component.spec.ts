import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaireInscriptionBeneficiaireComponent } from './formulaire-inscription-beneficiaire.component';

describe('FormulaireInscriptionBeneficiaireComponent', () => {
  let component: FormulaireInscriptionBeneficiaireComponent;
  let fixture: ComponentFixture<FormulaireInscriptionBeneficiaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormulaireInscriptionBeneficiaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaireInscriptionBeneficiaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
