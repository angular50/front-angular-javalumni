import { Component, OnInit } from '@angular/core';
import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'src/app/pages/inscription/CustomValidator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulaire-inscription-beneficiaire',
  templateUrl: './formulaire-inscription-beneficiaire.component.html',
  styleUrls: ['./formulaire-inscription-beneficiaire.component.scss']
})
export class FormulaireInscriptionBeneficiaireComponent implements OnInit {
  utilisateur: Utilisateur;
  inscriptionBenef: FormGroup;
  password: string;

  constructor(private utilisateurService: UtilisateurHttpService, private fb: FormBuilder,
    private router : Router) {
    this.inscriptionBenef = fb.group({
      numPE: localStorage.getItem("numPE"),
      nom:"",
      prenom: "",
      presentation:"",
      linkedin:"",
      email: [
        null,
        Validators.compose([Validators.email, Validators.required])
      ],
      password: [
      null,
      Validators.compose([
        Validators.required,
        // check whether the entered password has a number
        CustomValidators.patternValidator(/\d/, {
          hasNumber: true
        }),
        // check whether the entered password has upper case letter
        CustomValidators.patternValidator(/[A-Z]/, {
          hasCapitalCase: true
        }),
        // check whether the entered password has a lower case letter
        CustomValidators.patternValidator(/[a-z]/, {
          hasSmallCase: true
        }),
        // check whether the entered password has a special character
        CustomValidators.patternValidator(
          /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
          {
            hasSpecialCharacters: true
          }
        ),
        Validators.minLength(8)
      ])
    ],
      confirmPassword:[null, Validators.compose([Validators.required])]
    },
    {
      // check whether our password and confirm password match
      validator: CustomValidators.passwordMatchValidator
    }
    )
   }
 

  ngOnInit() {
    this.getbyId(localStorage.getItem('numPE'));
  }

  inscriptionBeneficiaire=()=>{
    

    localStorage.setItem("nom", this.inscriptionBenef.value.nom);
    localStorage.setItem("prenom", this.inscriptionBenef.value.prenom);
    localStorage.setItem("presentation", this.inscriptionBenef.value.presentation);
    localStorage.setItem("linkedin", this.inscriptionBenef.value.linkedin);
    localStorage.setItem("email", this.inscriptionBenef.value.email);
    localStorage.setItem("password", this.inscriptionBenef.value.password);
    localStorage.setItem("idProjet", "1");


    let utilisateur = new Utilisateur;
    utilisateur.idutilisateur = JSON.parse(localStorage.getItem("idUtilisateur"));
    utilisateur.numPE = JSON.parse(localStorage.getItem("numPE"));
    utilisateur.promotion = localStorage.getItem("promotion");
    utilisateur.formation = localStorage.getItem("formation");
    utilisateur.dateDebut = JSON.parse(localStorage.getItem("dateDebut"));
    utilisateur.dateFin = JSON.parse(localStorage.getItem("dateFin"));
    utilisateur.nom = localStorage.getItem("nom");
    utilisateur.prenom = localStorage.getItem("prenom");
    utilisateur.presentation = localStorage.getItem("presentation");
    utilisateur.linkedin = localStorage.getItem("linkedin");
    utilisateur.email = localStorage.getItem("email");
    utilisateur.password = localStorage.getItem("password");
    utilisateur.projet.idprojet = 1;

    this.utilisateurService.modifUtilisateur(utilisateur).subscribe(
      ()=>{
        this.router.navigate(['/login']);
      },
      err => {console.warn(err);});
}

  getbyId(id:string) {
    this.utilisateurService.getUtilisateursByNumPE(id).subscribe(
      (utilisateur:Utilisateur)=> {
        this.utilisateur=utilisateur;
        localStorage.setItem("idUtilisateur", JSON.stringify(utilisateur.idutilisateur));
        localStorage.setItem("numPE", JSON.stringify(utilisateur.numPE));
        localStorage.setItem("formation", utilisateur.formation);
        localStorage.setItem("promotion", utilisateur.promotion);
        localStorage.setItem("dateDebut", JSON.stringify(utilisateur.dateDebut));
        localStorage.setItem("dateFin", JSON.stringify(utilisateur.dateFin));},
      err => {console.warn(err);}
    )
  }
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '15rem',
      
      maxHeight: 'auto',
      width: '53rem',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',
    
};
}
