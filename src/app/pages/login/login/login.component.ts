import { Component, OnInit } from '@angular/core';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthentificationService } from 'src/app/services/authentification/authentification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  utilisateur: Utilisateur;
  logForm: FormGroup;

  constructor(private fb: FormBuilder, private authService: AuthentificationService,
    private router:Router) {
    this.logForm = fb.group({
      email: "",
      password:""
   });
  }

  ngOnInit() {
    localStorage.clear();
  }
/*
  onclick() {
    let utilisateur = new Utilisateur;
    utilisateur.email=this.logForm.value.email;
    utilisateur.password=this.logForm.value.password;

    this.authService.login(utilisateur)

    if (localStorage.length === 0) {
      this.router.navigate(['/login']);
    } else {
      this.router.navigate(['/homePage']);
    }
  }
*/
  login=()=>{
    let utilisateur = new Utilisateur;
    utilisateur.email = this.logForm.value.email;
    utilisateur.password=this.logForm.value.password;

    this.authService.authentification(utilisateur).subscribe(
      (utilisateur:Utilisateur)=> {
        this.utilisateur=utilisateur;
        console.log(utilisateur);
        localStorage.setItem("idUtilisateur", JSON.stringify(utilisateur.idutilisateur));
        localStorage.setItem("email", utilisateur.email);
        localStorage.setItem("promotion", utilisateur.promotion);
        localStorage.setItem("projetSoutenu", JSON.stringify(utilisateur.projetSoutenu));
        localStorage.setItem("siteWeb", utilisateur.siteWeb);
        localStorage.setItem("nom", utilisateur.nom);
        localStorage.setItem("prenom", utilisateur.prenom);
        localStorage.setItem("nomEnt", utilisateur.nomEntreprise);

        localStorage.setItem("pres", utilisateur.presentation);
        localStorage.setItem("link", utilisateur.linkedin);
        localStorage.setItem("rcssoutenir", JSON.stringify(utilisateur.numRCS));

        localStorage.setItem("dateFinCheck",JSON.stringify(utilisateur.dateFin));
        //let a = JSON.parse(localStorage.getItem('projetSoutenu')).valueOf
        console.log(typeof(utilisateur.projetSoutenu))
        // localStorage.setItem("idProjet", JSON.stringify(utilisateur.projet.idprojet));
        if (localStorage.length === 0) {
          this.router.navigate(['/login']);
        } else {
          this.router.navigate(['/homePage']);
          
        }
      },
      err => {console.warn(err);});
    }
}
