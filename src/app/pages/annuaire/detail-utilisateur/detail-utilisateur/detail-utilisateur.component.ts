import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { Component, OnInit } from '@angular/core';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-utilisateur',
  templateUrl: './detail-utilisateur.component.html',
  styleUrls: ['./detail-utilisateur.component.scss']
})
export class DetailUtilisateurComponent implements OnInit {
  utilisateur:Utilisateur;
  id: number;
  verif:boolean;
  constructor(private route: ActivatedRoute, private utilisateurHttpService: UtilisateurHttpService) { }

  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.id= params.id;
    });

    this.getbyId(this.id);
  }

  getbyId(id:number) {
    this.utilisateurHttpService.getUtilisateurById(id).subscribe(
      (utilisateur:Utilisateur)=> {
        this.utilisateur=utilisateur;},
      err => {console.warn(err);}
    )
  }

}
