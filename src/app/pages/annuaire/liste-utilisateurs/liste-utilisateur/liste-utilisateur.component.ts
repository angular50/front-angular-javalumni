import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { Component, OnInit } from '@angular/core';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { AnnuaireGroup } from 'src/app/models/annuaireGroup/annuaire-group';

@Component({
  selector: 'app-liste-utilisateur',
  templateUrl: './liste-utilisateur.component.html',
  styleUrls: ['./liste-utilisateur.component.scss']
})
export class ListeUtilisateurComponent implements OnInit {
  p: number = 1;
  data:Utilisateur[];
  types = new Array<AnnuaireGroup>();
  constructor(private utilisateurService: UtilisateurHttpService) {}

  ngOnInit() {

    this.utilisateurService.getUtilisateurs().subscribe(
      (data:Utilisateur[])=> {
        this.data=data;
        console.log(data);},
      err => {console.warn(err);}
    )
  }

  orderByTypeBenef() {
    this.utilisateurService.getBeneficiaires().subscribe(
      (data:Utilisateur[])=> {
        this.data=data;},
      err => {console.warn(err);}
    )
  }

  orderByTypeEntre() {
    this.utilisateurService.getEntreprises().subscribe(
      (data:Utilisateur[])=> {
        this.data=data;},
      err => {console.warn(err);}
    )
  }

  orderByTypeBenefOld() {
    this.utilisateurService.orderByTypeBenefOld().subscribe(
      (data:Utilisateur[])=> {
        this.data=data;},
      err => {console.warn(err);}
    )
  }

  orderByTypeBenefNew() {
    this.utilisateurService.orderByTypeBenefNew().subscribe(
      (data:Utilisateur[])=> {
        this.data=data;},
      err => {console.warn(err);}
    )
  }
}
