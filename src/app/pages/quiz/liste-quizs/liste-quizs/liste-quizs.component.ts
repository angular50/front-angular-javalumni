import { CatGroup } from 'src/app/models/catGroup/cat-group';
import { Quiz } from './../../../../models/quiz/quiz';
import { QuizHttpService } from 'src/app/services/quiz/quiz-http.service';
import { Component, OnInit } from '@angular/core';
import { QuizGroup } from 'src/app/models/QuizGroup/quiz-group';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-liste-quizs',
  templateUrl: './liste-quizs.component.html',
  styleUrls: ['./liste-quizs.component.scss']
})
export class ListeQuizsComponent implements OnInit {
  data:Quiz[];
  data2:Quiz[];
  categories = new Array<QuizGroup>();
  verif: boolean;
  dateVerif = new Date;
  dateVerifUtilisateur:any;
  idUtilisateurLocal:number;
  mesQuizz:FormGroup;
  constructor(private quizHttpService: QuizHttpService,private fb:FormBuilder) {
    this.mesQuizz = fb.group({
    })
   }

  ngOnInit() {

    this.quizHttpService.getQuizs().subscribe(
      (data:Quiz[])=> {
        this.data=data;
        this.idUtilisateurLocal = JSON.parse(localStorage.getItem("idUtilisateur"));

       this.dateVerifUtilisateur = new Date (localStorage.getItem('dateFinCheck'))
       console.log(this.dateVerif);
       console.log(this.dateVerifUtilisateur)
          if( this.dateVerif > this.dateVerifUtilisateur ){
          this.verif=true;
        }else {
          this.verif=false;
        }
        ;},
      err => {console.warn(err);}
    )

    this.getCat();
  }

  getCat() {
    this.quizHttpService.getQuizsByCat().subscribe(
      (categorie:string[])=> {
        categorie.forEach(
          categorie =>
        this.quizHttpService.getQuizsByCatNb(categorie).subscribe(
          (count: number) => {
            this.categories.push(new QuizGroup(categorie, count))
          },
          err => {console.warn(err);}
        )
        )
        console.log(categorie);},
      err => {console.warn(err);}
    )
  }

  orderByCategorie(categorie:string) {
    console.log(categorie);
    this.quizHttpService.getQuizsByCatList(categorie).subscribe(
      (data:Quiz[])=> {
        this.data=data;
        console.log(data);},
      err => {console.warn(err);}
    )
  }

  quizzExistant(){
    this.data2=this.data;
    if (this.data2.length !== 0){
      return true;
    }else{
      return false
    }
  }

}
