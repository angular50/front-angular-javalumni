import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeQuizsComponent } from './liste-quizs.component';

describe('ListeQuizsComponent', () => {
  let component: ListeQuizsComponent;
  let fixture: ComponentFixture<ListeQuizsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeQuizsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeQuizsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
