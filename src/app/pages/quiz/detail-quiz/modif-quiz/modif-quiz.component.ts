import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { QuizHttpService } from 'src/app/services/quiz/quiz-http.service';
import { ActivatedRoute } from '@angular/router';
import { Quiz } from 'src/app/models/quiz/quiz';
import { Question } from 'src/app/models/question/question';
import { Reponse } from 'src/app/models/reponse/reponse';
import { ReponseGroup } from 'src/app/models/reponseGroup/reponse-group';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-modif-quiz',
  templateUrl: './modif-quiz.component.html',
  styleUrls: ['./modif-quiz.component.scss']
})
export class ModifQuizComponent implements OnInit {
  id: number;
  verif:boolean;
  quizId:Quiz;
  quiz: FormGroup;
  sum: number;
  questions:Question[];
  reponses:Reponse;
  reponsesGlobales= new Array<Reponse>();
  data = new Array<ReponseGroup>();
  data2 = new Array<ReponseGroup>();
  test1: string = '';
  result: string='';
  un:number;
  deux:number;
  trois:number;
  quatre:number;
  cinq:number;
  six:number;
  quizzForm: FormGroup;
  tabCategories =['Java','MySql']
tabDifficultes = ['Débutant','Intermédiaire','Expert']
  constructor(private fb: FormBuilder, private quizService: QuizHttpService,
    private route: ActivatedRoute) { 
      this.quizzForm = fb.group({
        nom:"",
        categories:"",
        difficulte:"",
        description:"",
      })
    }
    

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.id = parseInt(params.id);
      
    });

    this.quizService.getQuizByIdQuiz(this.id).subscribe(
      
        (quiz:Quiz) => {
          this.quizId=quiz;
          localStorage.setItem('date', JSON.stringify(this.quizId.dateParution));
          
  
        },
        err => {console.warn(err);} 
      )
    


    this.quizService.getQuesionsByIdquiz(this.id).subscribe(
      (questions:Question[])=> {
        this.questions=questions;
        this.quizService.getNbQstByIdQuiz(this.id).subscribe(
            (data:number[])=> {
              //console.log(data);
              data.forEach(
                data =>
                this.quizService.getIdRepByIdQuest(data).subscribe(
                  (data2: number[]) => {
                    //console.log(data2);
                    data2.forEach(
                      data2 =>
                    this.quizService.getRepByIdqstAndIdrep(data, data2).subscribe(
                      (reponses:Reponse)=> {
                        this.reponses=reponses;
                        this.reponsesGlobales.push(reponses);
                        //console.log(reponses);
                      },
                      err => {console.warn(err);}
                    ) // fin getRepByIdqstAndIdrep
                    ) // fin du data2.forEach
                  },
                  err => {console.warn(err);}
                ) // fin getNbRepByIdQst(data)
              ) // fin du data.forEach
            },
            err => {console.warn(err);}
          ) // fin getNbQstByIdQuiz
      },
      err => {console.warn(err);}
    )

    console.log(this.reponsesGlobales);
  }
  modifQuiz(){
    console.log(this.quizzForm.value);
    console.log()
    let quiz= new Quiz;
    quiz.idquiz = this.id;
    quiz.categorie = this.quizzForm.value.categories;
    quiz.dateParution = new Date(localStorage.getItem('date'));
    quiz.descriptif = this.quizzForm.value.description;
    quiz.difficulte = this.quizzForm.value.difficulte;
    quiz.nom = this.quizzForm.value.nom;
    quiz.idutilisateur = parseInt(localStorage.getItem('idUtilisateur'));
    quiz.utilisateur.idutilisateur = parseInt(localStorage.getItem('idUtilisateur'));
    console.log(quiz);

      this.quizService.saveOrUpdateQuizz(quiz).subscribe(
      ()=> {
       // if (localStorage.length !== 0) {
        //this.routeur.navigate(['/offres/liste-offres']);
       // }
      },
      err => {console.warn(err);});
    }
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '15rem',
      
      maxHeight: 'auto',
      width: '60rem',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [
        'undo',
        'redo',
        'strikeThrough',
        'subscript',
        'superscript',
        'justifyLeft',
        'justifyCenter',
        'justifyRight',
        'justifyFull',
        'indent',
        'outdent',
        'insertUnorderedList',
        'insertOrderedList',
        'heading',
        'fontName'
      ],
      [
      'fontSize',
      'textColor',
      'backgroundColor',
      'customClasses',
      'link',
      'unlink',
      'insertImage',
      'insertVideo',
      'insertHorizontalRule',
      'removeFormat',
      'toggleEditorMode'
    ]

  ]
    
};
}
