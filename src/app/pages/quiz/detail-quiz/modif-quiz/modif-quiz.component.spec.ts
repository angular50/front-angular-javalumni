import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifQuizComponent } from './modif-quiz.component';

describe('ModifQuizComponent', () => {
  let component: ModifQuizComponent;
  let fixture: ComponentFixture<ModifQuizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifQuizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifQuizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
