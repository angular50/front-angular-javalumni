import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { QuizHttpService } from 'src/app/services/quiz/quiz-http.service';
import { ActivatedRoute } from '@angular/router';
import { Question } from 'src/app/models/question/question';
import { Reponse } from 'src/app/models/reponse/reponse';

@Component({
  selector: 'app-modif-question',
  templateUrl: './modif-question.component.html',
  styleUrls: ['./modif-question.component.scss']
})
export class ModifQuestionComponent implements OnInit {
id:number;
question:Question;
reponses:Reponse[];
quizzForm: FormGroup;
repForm:FormGroup;
  constructor(private fb: FormBuilder, private quizService: QuizHttpService,
    private route: ActivatedRoute) { 
      this.quizzForm = fb.group({
        question:"",
        
      })
      this.repForm = fb.group({
        reponse1:"",
        reponse2:"",
        reponse3:"",
        
      
    })
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = parseInt(params.id);
      
      
    });

    this.quizService.getQuestionById(this.id).subscribe(
      
      (question:Question) => {
        this.question=question;
        localStorage.setItem('questionCat',question.categorie);
        localStorage.setItem('questionCor',question.corrige);
        localStorage.setItem('questionDif',question.difficulte);
        localStorage.setItem('idquiz',JSON.stringify(question.quiz.idquiz));
        

        console.log(question);
        
      },
      err => {console.warn(err);} 
    )

    this.quizService.getReponsesByIdquestion(this.id).subscribe(
      (reponse:Reponse[]) => {
        this.reponses=reponse;
        console.log(reponse);
        
      },
      err => {console.warn(err);} 

    )
  }
  modifQuestion(){
    let question = new Question;
    question.quiz.idquiz = parseInt(localStorage.getItem('idquiz'));
    question.idquestion = this.id;
    question.question = this.quizzForm.value.question;
    question.categorie = localStorage.getItem('questionCat');
    question.corrige = "";
    question.difficulte = localStorage.getItem('questionDif');

    
    
    console.log(question);
    this.quizService.saveOrUpdateQuestion(question).subscribe(
     ()=> {
    },
    err => {console.warn(err);}
    )
  }

  modifierReponse(id:number,idBonneReponse:number){
    let reponse = new Reponse;
   
    reponse.question.idquestion = this.id;
    reponse.idreponse = id;
    reponse.bonneReponse = idBonneReponse;
    if(idBonneReponse === 1){
      reponse.reponse = this.repForm.value.reponse1;
    }
    else if(idBonneReponse === -1){
      reponse.reponse = this.repForm.value.reponse2;
    }
    else if(idBonneReponse === 0){
      reponse.reponse = this.repForm.value.reponse3;
    }
    
     
    
    console.log(reponse);
    this.quizService.saveReponse(reponse).subscribe(
     ()=> {
    },
    err => {console.warn(err);}
    )
  }

  
  

}
