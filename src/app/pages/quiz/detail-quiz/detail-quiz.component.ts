import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Question } from 'src/app/models/question/question';
import { QuizHttpService } from 'src/app/services/quiz/quiz-http.service';
import { Reponse } from 'src/app/models/reponse/reponse';
import { ReponseGroup } from 'src/app/models/reponseGroup/reponse-group';
import { ActivatedRoute } from '@angular/router';
import { Quiz } from 'src/app/models/quiz/quiz';

@Component({
  selector: 'app-detail-quiz',
  templateUrl: './detail-quiz.component.html',
  styleUrls: ['./detail-quiz.component.scss']
})
export class DetailQuizComponent implements OnInit {
  id: number;
  verif:boolean;
  quizId:Quiz;
  quiz: FormGroup;
  sum: number;
  questions:Question[];
  reponses:Reponse;
  reponsesGlobales= new Array<Reponse>();
  data = new Array<ReponseGroup>();
  data2 = new Array<ReponseGroup>();
  test1: string = '';
  result: string='';
  un:number;
  deux:number;
  trois:number;
  quatre:number;
  cinq:number;
  six:number;
  constructor(private fb: FormBuilder, private quizService: QuizHttpService,
              private route: ActivatedRoute) {
    this.quiz = fb.group({
      choix1: ['', Validators.required],
      choix2: ['', Validators.required],
      choix3: ['', Validators.required],
      choix4: ['', Validators.required],
      choix5: ['', Validators.required],
      choix6: ['', Validators.required]
      });
  }

  ngOnInit(
  ) {

    this.route.params.subscribe(params => {
      this.id = parseInt(params.id);
      
    });

    this.quizService.getQuizByIdQuiz(this.id).subscribe(
      
        (quiz:Quiz) => {
          this.quizId=quiz;
          console.log(quiz.utilisateur.idutilisateur);
        console.log(localStorage.getItem('idUtilisateur'));
        if(JSON.stringify(quiz.utilisateur.idutilisateur)===(localStorage.getItem('idUtilisateur'))){
          this.verif=true;
        }else {
          this.verif=false;
        }
        console.log(this.verif);
  
        },
        err => {console.warn(err);} 
      )
    


    this.quizService.getQuesionsByIdquiz(this.id).subscribe(
      (questions:Question[])=> {
        this.questions=questions;
        this.quizService.getNbQstByIdQuiz(this.id).subscribe(
            (data:number[])=> {
              //console.log(data);
              data.forEach(
                data =>
                this.quizService.getIdRepByIdQuest(data).subscribe(
                  (data2: number[]) => {
                    //console.log(data2);
                    data2.forEach(
                      data2 =>
                    this.quizService.getRepByIdqstAndIdrep(data, data2).subscribe(
                      (reponses:Reponse)=> {
                        this.reponses=reponses;
                        this.reponsesGlobales.push(reponses);
                        //console.log(reponses);
                      },
                      err => {console.warn(err);}
                    ) // fin getRepByIdqstAndIdrep
                    ) // fin du data2.forEach
                  },
                  err => {console.warn(err);}
                ) // fin getNbRepByIdQst(data)
              ) // fin du data.forEach
            },
            err => {console.warn(err);}
          ) // fin getNbQstByIdQuiz
      },
      err => {console.warn(err);}
    )

    console.log(this.reponsesGlobales);
  }

  traiter() {

    this.un = +(this.quiz.value.choix1);
    this.deux = +(this.quiz.value.choix2);
    this.trois = +(this.quiz.value.choix3);
    this.quatre = +(this.quiz.value.choix4);
    this.cinq = +(this.quiz.value.choix5);
    this.six = +(this.quiz.value.choix6);
    this.sum = this.un + this.deux + this.trois + this.quatre + this.cinq + this.six;
    console.log(this.sum);

//Ici le traitement de la somme des values dans le form
    if (this.sum <= 2) {
          this.result= "Insuffisant, revoyez les bases avant de retentez le Quiz";
        } else {
      if (this.sum <= 4) {
          this.result= "Moyen, attention vous avez sûrement fait zappé quelque chose";
    } else {
        if (this.sum < 6) {
          this.result= "C'est pas mal !!!";
      } else {
          this.result= "Excellent";
      }
    }
    }
  }

}
