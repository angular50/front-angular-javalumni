import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QuizHttpService } from 'src/app/services/quiz/quiz-http.service';
import { Quiz } from 'src/app/models/quiz/quiz';
import { QuizGroup } from 'src/app/models/QuizGroup/quiz-group';

@Component({
  selector: 'app-mes-quiz',
  templateUrl: './mes-quiz.component.html',
  styleUrls: ['./mes-quiz.component.scss']
})
export class MesQuizComponent implements OnInit {
id:number;
data:Quiz[];
pasQuiz: boolean;
categories = new Array<QuizGroup>();
idUtilisateurLocal:number;
dateVerifUtilisateur:any;
dateVerif = new Date;
verif: boolean;
  constructor(private route: ActivatedRoute, private quizService: QuizHttpService) { }

  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.id= params.id;
  })
  this.listeMesQuiz();
  this.getCat();
  this.pasQuiz = JSON.parse(localStorage.getItem("quiz")) !== 0;


  }
  listeMesQuiz() {
    this.quizService.getQuizByIdUtilisateur(this.id).subscribe(
      (data:Quiz[])=> {
        this.data=data;
        localStorage.setItem("quiz", JSON.stringify(data.length));
        this.idUtilisateurLocal = JSON.parse(localStorage.getItem("idUtilisateur"));

       this.dateVerifUtilisateur = new Date (localStorage.getItem('dateFinCheck'))
       console.log(this.dateVerif);
       console.log(this.dateVerifUtilisateur)
          if( this.dateVerif > this.dateVerifUtilisateur ){
          this.verif=true;
        }else {
          this.verif=false;
        }
        console.log(this.pasQuiz)

        localStorage.setItem("offre", JSON.stringify(data.length));
        console.log(data);},
      err => {console.warn(err);}
    )
  }

  getCat() {
    this.quizService.getQuizsByCat().subscribe(
      (categorie:string[])=> {
        categorie.forEach(
          categorie =>
        this.quizService.getQuizsByCatNb(categorie).subscribe(
          (count: number) => {
            this.categories.push(new QuizGroup(categorie, count))
          },
          err => {console.warn(err);}
        )
        )
        console.log(categorie);},
      err => {console.warn(err);}
    )
  }

}
