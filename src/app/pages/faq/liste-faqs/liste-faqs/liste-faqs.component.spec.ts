import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeFaqsComponent } from './liste-faqs.component';

describe('ListeFaqsComponent', () => {
  let component: ListeFaqsComponent;
  let fixture: ComponentFixture<ListeFaqsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeFaqsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeFaqsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
