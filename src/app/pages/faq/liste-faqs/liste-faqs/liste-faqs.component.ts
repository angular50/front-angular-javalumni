import { FaqHttpService } from './../../../../services/faq/faq-http.service';
import { Component, OnInit } from '@angular/core';
import { Faq } from 'src/app/models/faq/faq';
import { Reponse } from 'src/app/models/reponse/reponse';
import { ReponseFAQ } from 'src/app/models/reponseFAQ/reponse-faq';
import { FaqGroup } from 'src/app/models/faqGroup/faq-group';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-liste-faqs',
  templateUrl: './liste-faqs.component.html',
  styleUrls: ['./liste-faqs.component.scss']
})
export class ListeFaqsComponent implements OnInit {
  p: number = 1;
  faqs:Faq[];
  data=new Array<FaqGroup>();
  data2=new Array<FaqGroup>();
  thematiques = new Array<FaqGroup>();
  reponses:ReponseFAQ;
  reponsesGlobales= new Array<ReponseFAQ>();
  ajoutqstFAQ: FormGroup;
  catThematique=['Site', 'Metier', 'IDP_Partenaires', 'Autre']
  //types = new Array<FaqGroup>();
  constructor(private faqHttpService: FaqHttpService, private fb:FormBuilder){ 
    this.ajoutqstFAQ = fb.group({
      thematique:"",
      question:"",
      idutilisateur:"",
    })
    }

  ngOnInit() {

    /*
    this.faqHttpService.getFaqs().subscribe(
      (faqs:Faq[])=> {
        this.faqs=faqs;},
      err => {console.warn(err);}
    )
*/

    this.getThematique()

    this.faqHttpService.getFaqs().subscribe(
      (faqs:Faq[])=> {
        this.faqs=faqs;
        this.faqHttpService.getNbRepByIdFaq().subscribe(
            (data:number[])=> {
              //console.log(data);
              data.forEach(
                data =>
                this.faqHttpService.getIdRepByIdFaq(data).subscribe(
                  (data2: number[]) => {
                    //console.log(data2);
                    data2.forEach(
                      data2 =>
                    this.faqHttpService.getRepByIdFaqAndIdrep(data, data2).subscribe(
                      (reponses:ReponseFAQ)=> {
                        this.reponses=reponses;
                        this.reponsesGlobales.push(reponses);
                        //console.log(reponses);
                      },
                      err => {console.warn(err);}
                    ) // fin getRepByIdqstAndIdrep
                    ) // fin du data2.forEach
                  },
                  err => {console.warn(err);}
                ) // fin getNbRepByIdQst(data)
              ) // fin du data.forEach
            },
            err => {console.warn(err);}
          ) // fin getNbQstByIdQuiz
      },
      err => {console.warn(err);}
    )

  }
  
  
  getThematique() {
    this.faqHttpService.getFaqsByThematique().subscribe(
      (thematique:string[])=> {
        console.log(thematique);
        thematique.forEach(
          thematique =>
        this.faqHttpService.getFaqsByThematiqueNb(thematique).subscribe(
          (count: number) => {
            this.thematiques.push(new FaqGroup(thematique, count))
          },
          err => {console.warn(err);}
        )
        )},
      err => {console.warn(err);}
    )
  }


  saveQstFAQ=()=>{
    let faq= new Faq;
    faq.thematique= this.ajoutqstFAQ.value.thematique;
    faq.question= this.ajoutqstFAQ.value.question;
    faq.utilisateur.idutilisateur = JSON.parse(localStorage.getItem("idUtilisateur"));
   
      this.faqHttpService.saveFaq(faq).subscribe(
      ()=> {
      },
      err => {console.warn(err);});
    }

}
