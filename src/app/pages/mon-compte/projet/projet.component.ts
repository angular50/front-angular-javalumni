import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { Component, OnInit } from '@angular/core';
import { Projet } from 'src/app/models/projet/projet';
import { ProjetHttpService } from 'src/app/services/projet/projet-http.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-projet',
  templateUrl: './projet.component.html',
  styleUrls: ['./projet.component.scss']
})
export class ProjetComponent implements OnInit {
  projet:Projet;
  utilProjet:Utilisateur
  utilisateur: Utilisateur;
  veriNomGroupe: FormGroup;
  modificationProjet: FormGroup;
  idOK: number;
  projetSoutenu: number;
  techno: string;
  fonctions: string;
  tabtechnosbacks=['Java','PHP','Python','C#'];
  tabtechnosfronts=['Angular','IONIC','JEE','REACT'];
  tabtechnosbases=['MySQL','NoSQL','PostGre','MongoDB'];
  splitted:string[];
  splitfonc:string[];
  etatActuel:string;
  etats=['Terminé','En Cours'];

  constructor(private fb: FormBuilder, private projetService: ProjetHttpService,
    // tslint:disable-next-line: align
    private utilisateurService: UtilisateurHttpService) {
    this.veriNomGroupe = fb.group({
      groupe: ""
   })

   this.modificationProjet = fb.group({
    titre:"",
    description:"",
    groupe:"",
    technosback:"",
    technosfront:"",
    technosbases:"",
    fonctionnalites1:"",
    fonctionnalites2:"",
    fonctionnalites3:"",
    etat:"",
    git1: "",
    git2:""
  
   })
  }
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '15rem',
      
      maxHeight: 'auto',
      width: '50rem',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',
    
};
  ngOnInit() {

    this.projetSoutenu= JSON.parse(localStorage.getItem('projetSoutenu'));

    /*J'attribue pour l'id le projetSoutenu
    car je ne sais pas comment recuperer la FK de utilisateur
    */
   this.idOK = JSON.parse(localStorage.getItem('idProjet'));
   this.getbyId(this.idOK);
   this.utilisateurService.getUtilisateurById(parseInt(localStorage.getItem('idUtilisateur'))).subscribe(
    (utilisateur: Utilisateur) => {
      this.utilProjet = utilisateur;
      console.log(utilisateur); },
    err => {console.warn(err); }
  );
  }

  verifNomGroupe=()=>{
    let projet = new Projet;
    projet.groupe = this.veriNomGroupe.value.groupe;
    this.projetService.verifNomGroupe(projet).subscribe(
      (projet:Projet)=> {
        this.projet=projet;
        localStorage.setItem("idProjet", JSON.stringify(projet.idprojet));
        console.log("Je suis le projet numero : " +localStorage.getItem('idProjet'));

        // tslint:disable-next-line: new-parens
        this.utilisateur = new Utilisateur;
        this.utilisateur.idutilisateur = JSON.parse(localStorage.getItem('idUtilisateur'));
        this.utilisateur.nom = this.utilProjet.nom;
        this.utilisateur.prenom = this.utilProjet.prenom;
        this.utilisateur.presentation = this.utilProjet.presentation
        this.utilisateur.numPE = JSON.parse(localStorage.getItem('numPE'));
        this.utilisateur.promotion = localStorage.getItem('promotion');
        this.utilisateur.email = JSON.parse(localStorage.getItem('email'));
        this.utilisateur.password = JSON.parse(localStorage.getItem('password'));
        this.utilisateur.formation = this.utilProjet.formation;
        this.utilisateur.dateDebut = this.utilProjet.dateDebut;
        this.utilisateur.dateFin = this.utilProjet.dateFin;
        this.utilisateur.linkedin = this.utilProjet.linkedin;
        this.idOK = JSON.parse(localStorage.getItem('idProjet'));
        console.log(this.idOK);
        this.utilisateur.projet.idprojet = this.idOK;

        this.utilisateurService.modifUtilisateur(this.utilisateur).subscribe(
      () => {
      },
      err => {console.warn(err);});


      },
      err => {console.warn(err);});
    }

  getbyId(id:number) {
    this.projetService.getProjetsById(id).subscribe(
      (projet:Projet)=> {
        this.projet=projet;
        let str=this.projet.technologies;
        this.splitted = str.split("/", 3); 
        let strfonc=this.projet.fonctionnalites;
        this.splitfonc= strfonc.split("/", 3);
        this.etatActuel = this.projet.etat;
        console.log(this.etatActuel);
        console.log(this.splitted);
        console.log(this.splitfonc);
      
      },
      err => {console.warn(err);}
    )
    
  }

  verifIdProjet() {
    if ((JSON.parse(localStorage.getItem('projetSoutenu')) === null &&
        (JSON.parse(localStorage.getItem('numRCS')) === null &&
        (JSON.parse(localStorage.getItem('idProjet')) === 1 )))) {
      console.log("true");
      return true;
    } else {
      console.log("false");
      return false;
    }
  }


  entreprise() {
    if ((JSON.parse(localStorage.getItem('numRCS')) > 1 )) {
      console.log("true");
      return true;
    } else {
      console.log("false");
      return false;
    }
  }


  verifSiEntreprise() {
    if ((JSON.parse(localStorage.getItem('numRCS')) > 1 || JSON.parse(localStorage.getItem('idProjet')) === 1)) {
      console.log("true");
      return true;
    } else {
      console.log("false");
      return false;
    }
  }

  verifSiEntrepriseAvecProjet() {
    if ((JSON.parse(localStorage.getItem('numRCS')) > 1 &&
        (JSON.parse(localStorage.getItem('projetSoutenu')) > 1))){
      console.log("true");
      return true;
    } else {
      console.log("false");
      return false;
    }
  }

  modifProjet(){
    
    
    this.techno = (this.modificationProjet.value.technosback + "/" + this.modificationProjet.value.technosfront + "/" + this.modificationProjet.value.technosbases);
    this.fonctions = (this.modificationProjet.value.fonctionnalites1 + "/" + this.modificationProjet.value.fonctionnalites2 + "/" + this.modificationProjet.value.fonctionnalites3);
    console.log(this.techno);
    console.log(this.fonctions);
    
    let projet= new Projet;
    
    projet.idprojet = parseInt(localStorage.getItem("idProjet"));
    projet.titre = this.modificationProjet.value.titre;
    projet.technologies = this.techno;
    projet.fonctionnalites = this.fonctions;
    projet.description = this.modificationProjet.value.description;
    projet.groupe = this.modificationProjet.value.groupe;
    projet.etat = this.modificationProjet.value.etat;
    console.log(projet);
    this.projetService.modifProjet(projet).subscribe(
      ()=> {
       // if (localStorage.length !== 0) {
        //this.routeur.navigate(['/offres/liste-offres']);
       // }
      },
      err => {console.warn(err);});
  }
}
