import { Component, OnInit } from '@angular/core';
import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { CustomValidators } from '../../inscription/CustomValidator';

@Component({
  selector: 'app-mon-profil',
  templateUrl: './mon-profil.component.html',
  styleUrls: ['./mon-profil.component.scss']
})
export class MonProfilComponent implements OnInit {
  utilisateur:Utilisateur;
  modificationBenef: FormGroup;
  tabvilles=['Lille', 'Lyon', 'Paris', 'Roubaix', 'Tourcoing'];

  constructor(private utilisateurService: UtilisateurHttpService, private fb:FormBuilder ) { 
this.modificationBenef = fb.group({
  nom:"",
  prenom:"",
  presentation:"",
  ville:"",
  linkedin:"",
  email:"",
  siteWeb:"",
})
}

editorConfig: AngularEditorConfig = {
  editable: true,
    spellcheck: true,
    height: '15rem',
    
    maxHeight: 'auto',
    width: '50rem',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      {class: 'arial', name: 'Arial'},
      {class: 'times-new-roman', name: 'Times New Roman'},
      {class: 'calibri', name: 'Calibri'},
      {class: 'comic-sans-ms', name: 'Comic Sans MS'}
    ],
    customClasses: [
    {
      name: 'quote',
      class: 'quote',
    },
    {
      name: 'redText',
      class: 'redText'
    },
    {
      name: 'titleText',
      class: 'titleText',
      tag: 'h1',
    },
  ],
  uploadUrl: 'v1/image',
  sanitize: true,
  toolbarPosition: 'top',
    
};
  ngOnInit() {

    let numDuProjet = localStorage.getItem("numProjet")
    localStorage.setItem("numProjet", numDuProjet);
    this.getbyId(localStorage.getItem('idUtilisateur'));
    
  }

  getbyId(id:string) {
    this.utilisateurService.getUtilisateursById(id).subscribe(
      (utilisateur:Utilisateur)=> {
        this.utilisateur=utilisateur;
        localStorage.setItem("idProjet", JSON.stringify(utilisateur.projet.idprojet));
        localStorage.setItem("date_debut",JSON.stringify(utilisateur.dateDebut));
        localStorage.setItem("date_fin",JSON.stringify(utilisateur.dateFin));
        localStorage.setItem("formation",utilisateur.formation);
        localStorage.setItem("numpe",JSON.stringify(utilisateur.numPE));
        localStorage.setItem("numrcs",JSON.stringify(utilisateur.numRCS));
        localStorage.setItem("projet_soutenu",JSON.stringify(utilisateur.projetSoutenu));
        localStorage.setItem("nom_entreprise",utilisateur.nomEntreprise);
        localStorage.setItem("ville",utilisateur.ville);
        localStorage.setItem("password",utilisateur.password);
        console.log(utilisateur);

      
      },
      err => {console.warn(err);}
    )
  }

  modifCompte=()=>{
    console.log(this.modificationBenef.value);
    let utilisateur= new Utilisateur;
    utilisateur.idutilisateur = parseInt(localStorage.getItem('idUtilisateur'));
    utilisateur.prenom= this.modificationBenef.value.prenom;
    utilisateur.email= this.modificationBenef.value.email;
    utilisateur.nom= this.modificationBenef.value.nom;
    utilisateur.presentation = this.modificationBenef.value.presentation;
    utilisateur.linkedin = this.modificationBenef.value.linkedin;
    utilisateur.dateDebut = new Date(localStorage.getItem('date_debut'));
    utilisateur.dateFin = new Date(localStorage.getItem('date_fin'));
    utilisateur.formation = localStorage.getItem('formation');
    utilisateur.numPE = parseInt(localStorage.getItem('numPE'));
    utilisateur.numRCS = parseInt(localStorage.getItem('numRCS'));
    utilisateur.siteWeb = this.modificationBenef.value.siteWeb;
    utilisateur.projetSoutenu = parseInt(localStorage.getItem('projet_soutenu'));
    utilisateur.ville = this.modificationBenef.value.ville;
    utilisateur.promotion = localStorage.getItem('promotion');
    utilisateur.password = localStorage.getItem('password');
    utilisateur.projet.idprojet = parseInt(localStorage.getItem('idProjet'))

      this.utilisateurService.modifUtilisateur(utilisateur).subscribe(
      ()=> {
       // if (localStorage.length !== 0) {
        //this.routeur.navigate(['/offres/liste-offres']);
       // }
      },
      err => {console.warn(err);});
    }


}
