import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CustomValidators } from 'src/app/pages/inscription/CustomValidator';
import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modif-mdp',
  templateUrl: './modif-mdp.component.html',
  styleUrls: ['./modif-mdp.component.scss']
})
export class ModifMdpComponent implements OnInit {
  utilisateur:Utilisateur;
  modificationMdp: FormGroup;

  constructor(
    private fb:FormBuilder, 
    private utilisateurService: UtilisateurHttpService, 
    private router:Router)
     {
    this.modificationMdp = fb.group({
      password: [
      null,
      Validators.compose([
        Validators.required,
        // check whether the entered password has a number
        CustomValidators.patternValidator(/\d/, {
          hasNumber: true
        }),
        // check whether the entered password has upper case letter
        CustomValidators.patternValidator(/[A-Z]/, {
          hasCapitalCase: true
        }),
        // check whether the entered password has a lower case letter
        CustomValidators.patternValidator(/[a-z]/, {
          hasSmallCase: true
        }),
        // check whether the entered password has a special character
        CustomValidators.patternValidator(
          /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
          {
            hasSpecialCharacters: true
          }
        ),
        Validators.minLength(8)
      ])
    ],
      confirmPassword:[null, Validators.compose([Validators.required])]
    },
    {
      // check whether our password and confirm password match
      validator: CustomValidators.passwordMatchValidator
    }
    )

  }
     

  ngOnInit() {
    let numDuProjet = localStorage.getItem("numProjet")
    localStorage.setItem("numProjet", numDuProjet);
    this.getbyId(localStorage.getItem('idUtilisateur'));
  }

  getbyId(id:string) {
    this.utilisateurService.getUtilisateursById(id).subscribe(
      (utilisateur:Utilisateur)=> {
        this.utilisateur=utilisateur;
        localStorage.setItem("idProjet", JSON.stringify(utilisateur.projet.idprojet));
        localStorage.setItem('presentation',(utilisateur.presentation));
        localStorage.setItem("date_debut",JSON.stringify(utilisateur.dateDebut));
        localStorage.setItem("date_fin",JSON.stringify(utilisateur.dateFin));
        localStorage.setItem('linkedin',JSON.stringify(utilisateur.linkedin));
        localStorage.setItem("formation",utilisateur.formation);
        localStorage.setItem("numpe",JSON.stringify(utilisateur.numPE));
        localStorage.setItem("numrcs",JSON.stringify(utilisateur.numRCS));
        
        localStorage.setItem("nom_entreprise",utilisateur.nomEntreprise);
        localStorage.setItem("ville",utilisateur.ville);
        console.log(utilisateur);
      
      },
      err => {console.warn(err);}
    )
  }

  modifMdp=()=>{
    
    let utilisateur= new Utilisateur;
    utilisateur.idutilisateur = parseInt(localStorage.getItem('idUtilisateur'));
    utilisateur.prenom= localStorage.getItem('prenom');
    utilisateur.email= JSON.parse(localStorage.getItem('email'));
    utilisateur.nom= localStorage.getItem('nom');
    utilisateur.presentation = localStorage.getItem('presentation');
    utilisateur.linkedin = JSON.parse(localStorage.getItem('linkedin'));
    utilisateur.dateDebut = new Date(localStorage.getItem('date_debut'));
    utilisateur.dateFin = new Date(localStorage.getItem('date_fin'));
    utilisateur.formation = localStorage.getItem('formation');
    utilisateur.numPE = parseInt(localStorage.getItem('numPE'));
    utilisateur.numRCS = parseInt(localStorage.getItem('numRCS'));
    utilisateur.siteWeb = localStorage.getItem('siteWeb');
    utilisateur.projetSoutenu = parseInt(localStorage.getItem('projet_soutenu'));
    utilisateur.ville = localStorage.getItem('ville');
    utilisateur.promotion = localStorage.getItem('promotion');
    utilisateur.password = this.modificationMdp.value.password;
    utilisateur.projet.idprojet = parseInt(localStorage.getItem('idProjet'))
    console.log(utilisateur);

      this.utilisateurService.modifUtilisateur(utilisateur).subscribe(
      ()=> {
       // if (localStorage.length !== 0) {
        //this.routeur.navigate(['/offres/liste-offres']);
       // }
      },
      err => {console.warn(err);});
    }
}

  





