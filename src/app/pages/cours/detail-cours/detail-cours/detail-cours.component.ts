import { Component, OnInit } from '@angular/core';
import { PdfViewerComponent } from 'ng2-pdf-viewer';

@Component({
  selector: 'app-detail-cours',
  templateUrl: './detail-cours.component.html',
  styleUrls: ['./detail-cours.component.scss']
})
export class DetailCoursComponent implements OnInit {
  src = "/assets/pdf/JavaLesBases.pdf";
  constructor() { }

  ngOnInit() {
  }

}
