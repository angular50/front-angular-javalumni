import { Component, OnInit } from '@angular/core';
import { HomePageService } from 'src/app/services/homePage/home-page.service';
import { Article } from 'src/app/models/article/article';
import { Evenement } from 'src/app/models/evenement/evenement';
import { Offre } from 'src/app/models/offre/offre';
import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
article: Article;
evenement: Evenement;
utilisateur:Utilisateur;
offre: Offre;
  constructor(private homePageService: HomePageService, private utilisateurService: UtilisateurHttpService) { }

  ngOnInit() {

    this.getbyId(localStorage.getItem('idUtilisateur'));

    this.homePageService.getLastArticle().subscribe(
      (article:Article)=> {
        this.article=article;
        console.log(article);},
      err => {console.warn(err);}
    )

    this.homePageService.getLastEvenement().subscribe(
      (evenement:Evenement)=> {
        this.evenement=evenement;
        console.log(evenement);},
      err => {console.warn(err);}
    )

    this.homePageService.getLastOffre().subscribe(
      (offre:Offre)=> {
        this.offre=offre;
        console.log(offre);},
      err => {console.warn(err);}
    )

  }

  getbyId(id:string) {
    this.utilisateurService.getUtilisateursById(id).subscribe(
      (utilisateur:Utilisateur)=> {
        this.utilisateur=utilisateur;
        localStorage.setItem("idProjet", JSON.stringify(utilisateur.projet.idprojet));
        localStorage.setItem("numRCS", JSON.stringify(utilisateur.numRCS));
        localStorage.setItem("numPE", JSON.stringify(utilisateur.numPE));
        localStorage.setItem("email", JSON.stringify(utilisateur.email));
        localStorage.setItem("password", JSON.stringify(utilisateur.password));
      },
      err => {console.warn(err);}
    )
  }
}
