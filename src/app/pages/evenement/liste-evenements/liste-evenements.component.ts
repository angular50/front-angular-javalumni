import { Component, OnInit } from '@angular/core';
import { Evenement } from 'src/app/models/evenement/evenement';
import { VilleGroup } from 'src/app/models/villeGroup/ville-group';
import { EvenementHttpService } from 'src/app/services/evenement/evenement-http.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-liste-evenements',
  templateUrl: './liste-evenements.component.html',
  styleUrls: ['./liste-evenements.component.scss']
})
export class ListeEvenementsComponent implements OnInit {
  p: number = 1;
  villes = new Array<VilleGroup>();
  id:number;
  data:Evenement[];
  data2:Evenement[];
  idUtilisateurLocal: number;
  mesEvenements : FormGroup;
  constructor(private fb:FormBuilder, private evenementService: EvenementHttpService) { 
    this.mesEvenements = fb.group({      
    })
  }

  ngOnInit() {
    this.idUtilisateurLocal = JSON.parse(localStorage.getItem("idUtilisateur"));
    this.evenementService.getEvenements().subscribe(
      (data:Evenement[])=> {
        this.data=data;
        console.log(data);},
      err => {console.warn(err);}
    )

    this.getEtat();
    this.EvenementExistant();

  }
  listeMesEvenements() {
    let idDeMonUtilisateur = JSON.parse(localStorage.getItem("idUtilisateur"));
    this.evenementService.getEvenementsByIdUtilisateur(idDeMonUtilisateur).subscribe(
      (data:Evenement[])=> {
        this.data=data;
        localStorage.setItem("evenement", JSON.stringify(data.length));
        console.log(data);},
      err => {console.warn(err);}
    )
  }
  EvenementExistant(){
    this.data2=this.data;
    if ((this.data2).length !== 0){
      return true;
    }else{
      return false
    }
  }

  getEtat() {
    this.evenementService.getEvenementsByVille().subscribe(
      (ville:string[])=> {
        ville.forEach(
          ville =>
        this.evenementService.getEvenementsByVilleNb(ville).subscribe(
          (count: number) => {
            this.villes.push(new VilleGroup(ville, count))
          },
          err => {console.warn(err);}
        )
        )
        console.log(ville);},
      err => {console.warn(err);}
    )
  }

  orderByVille(ville:string) {
    console.log(ville);
    this.evenementService.getEvenementsByVilleList(ville).subscribe(
      (data:Evenement[])=> {
        this.data=data;
        console.log(data);},
      err => {console.warn(err);}
    )
  }

}

