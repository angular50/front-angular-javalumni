import { Component, OnInit } from '@angular/core';
import { Evenement } from 'src/app/models/evenement/evenement';
import { ActivatedRoute } from '@angular/router';
import { EvenementHttpService } from 'src/app/services/evenement/evenement-http.service';

@Component({
  selector: 'app-detail-evenement',
  templateUrl: './detail-evenement.component.html',
  styleUrls: ['./detail-evenement.component.scss']
})
export class DetailEvenementComponent implements OnInit {
  evenement:Evenement;
  id: number;
  verif : boolean;
  constructor(private route: ActivatedRoute, private evenementService: EvenementHttpService) { }

  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.id= params.id;
    });

    this.getbyId(this.id);
    //this.verifUtilisateurEvenement(this.evenement);
  }

  getbyId(id:number) {
    this.evenementService.getEvenementsById(id).subscribe(
      (evenement:Evenement)=> {
        this.evenement=evenement;
        console.log(this.evenement.utilisateur.idutilisateur);
        console.log(localStorage.getItem('idUtilisateur'));
        if(JSON.stringify(evenement.utilisateur.idutilisateur)===(localStorage.getItem('idUtilisateur'))){
          this.verif=true;
        }else {
          this.verif=false;
        }
        console.log(this.verif);
  },
  err => {console.warn(err);}
)
  }
}
