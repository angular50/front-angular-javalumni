import { Component, OnInit } from '@angular/core';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EvenementHttpService } from 'src/app/services/evenement/evenement-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Evenement } from 'src/app/models/evenement/evenement';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-modifier-evenement',
  templateUrl: './modifier-evenement.component.html',
  styleUrls: ['./modifier-evenement.component.scss']
})
export class ModifierEvenementComponent implements OnInit {
  evenement : Evenement;
  modificationEvent : FormGroup;
  id:number;
  villes=['Lille', 'Lomme', 'Paris', 'Roubaix', 'Tourcoing']

  constructor(private fb:FormBuilder, 
    private evenementService: EvenementHttpService, 
    private route:ActivatedRoute) { 
      this.modificationEvent = fb.group({
        nom:String,
        description:String,
        localisation:String,
        dateEvenement:Date
         }
      )
    }
    editorConfig: AngularEditorConfig = {
      editable: true,
        spellcheck: true,
        height: '15rem',
        
        maxHeight: 'auto',
        width: '50rem',
        minWidth: '0',
        translate: 'yes',
        enableToolbar: true,
        showToolbar: true,
        placeholder: 'Enter text here...',
        defaultParagraphSeparator: '',
        defaultFontName: '',
        defaultFontSize: '',
        fonts: [
          {class: 'arial', name: 'Arial'},
          {class: 'times-new-roman', name: 'Times New Roman'},
          {class: 'calibri', name: 'Calibri'},
          {class: 'comic-sans-ms', name: 'Comic Sans MS'}
        ],
        customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText'
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      uploadUrl: 'v1/image',
      sanitize: true,
      toolbarPosition: 'top',
      
  };
    ngOnInit() {
      this.route.params.subscribe(params=>{
        this.id= params.id;
      });
      this.getbyId(this.id);
      //this.verifUtilisateurEvenement(this.evenement);
    }  

  getbyId(id:number) {
    this.evenementService.getEvenementsById(id).subscribe(
      (evenement:Evenement)=> {
        this.evenement=evenement;
        console.log(evenement);
        localStorage.setItem("idevenement", JSON.stringify(evenement.idevenement))
  },
  err => {console.warn(err);}
)
  }

  modifEvent=()=>{
    console.log(this.modificationEvent.value);
    let evenement= new Evenement;
    evenement.idevenement = parseInt(localStorage.getItem('idevenement'))
    evenement.nom= this.modificationEvent.value.nom;
    evenement.description= this.modificationEvent.value.description;
    evenement.localisation= this.modificationEvent.value.localisation;
    evenement.dateEvenement = this.modificationEvent.value.dateEvenement;
    evenement.utilisateur.idutilisateur = JSON.parse(localStorage.getItem('idUtilisateur'));

      this.evenementService.saveEvenement(evenement).subscribe(
      ()=> {
       // if (localStorage.length !== 0) {
        //this.routeur.navigate(['/offres/liste-offres']);
       // }
      },
      err => {console.warn(err);});
    }

  
}
