import { Component, OnInit } from '@angular/core';
import { Evenement } from 'src/app/models/evenement/evenement';
import { ActivatedRoute } from '@angular/router';
import { EvenementHttpService } from 'src/app/services/evenement/evenement-http.service';
import { VilleGroup } from 'src/app/models/villeGroup/ville-group';

@Component({
  selector: 'app-mes-evenements',
  templateUrl: './mes-evenements.component.html',
  styleUrls: ['./mes-evenements.component.scss']
})
export class MesEvenementsComponent implements OnInit {
  villes = new Array<VilleGroup>();
  id: number;
  data: Evenement[];
  pasEvenements: boolean;

  constructor(private route: ActivatedRoute, private evenementService: EvenementHttpService) { }



  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
  });
    this.listeMesEvenements();
    this.pasEvenements = JSON.parse(localStorage.getItem('evenement')) !== 0;
    this.getEtat();
}
listeMesEvenements() {
  this.evenementService.getEvenementsByIdUtilisateur(this.id).subscribe(
    (data: Evenement[]) => {
      this.data = data;
      localStorage.setItem('evenement', JSON.stringify(data.length));
      console.log(data); },
    err => {console.warn(err); }
  )
}
getEtat() {
  this.evenementService.getEvenementsByVille().subscribe(
    (ville: string[]) => {
      ville.forEach(
        ville =>
      this.evenementService.getEvenementsByVilleNb(ville).subscribe(
        (count: number) => {
          this.villes.push(new VilleGroup(ville, count));
        },
        err => {console.warn(err);}
      )
      );
      console.log(ville); },
    err => {console.warn(err); }
  )
}

orderByVille(ville: string) {
  console.log(ville);
  this.evenementService.getEvenementsByVilleList(ville).subscribe(
    (data: Evenement[]) => {
      this.data = data;
      console.log(data); },
    err => {console.warn(err); }
  );
}

}
