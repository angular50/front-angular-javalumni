import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/models/article/article';
import { ActivatedRoute } from '@angular/router';
import { ArticleHttpService } from 'src/app/services/article/article-http.service';
import { isEmptyExpression } from '@angular/compiler';
import { CatGroup } from 'src/app/models/catGroup/cat-group';

@Component({
  selector: 'app-mes-articles',
  templateUrl: './mes-articles.component.html',
  styleUrls: ['./mes-articles.component.scss']
})
export class MesArticlesComponent implements OnInit {
  cats = new Array<CatGroup>();
  id: number;
  data:Article[];
  pasArticles: boolean;
  mySubscription: any;
  constructor(private route: ActivatedRoute, private articleService: ArticleHttpService) { }

  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.id= params.id;
    });

    this.listeMesArticles();
    this.getEtat();
    this.pasArticles = JSON.parse(localStorage.getItem("article")) !== 0;
  }

  listeMesArticles() {
    this.articleService.getArticlesByIdUtilisateur(this.id).subscribe(
      (data:Article[])=> {
        this.data=data;
        localStorage.setItem("article", JSON.stringify(data.length));
        console.log(data);},
      err => {console.warn(err);}
    )
  }
  getEtat() {
    this.articleService.getArticlesByCat().subscribe(
      (cat:string[])=> {
        cat.forEach(
          cat =>
        this.articleService.getArticlesByCatNb(cat).subscribe(
          (count: number) => {
            this.cats.push(new CatGroup(cat, count))
          },
          err => {console.warn(err);}
        )
        )
        console.log(cat);},
      err => {console.warn(err);}
    )
  }

  orderByCat(cat:string) {
    console.log(cat);
    this.articleService.getArticlesByCatList(cat).subscribe(
      (data:Article[])=> {
        this.data=data;
        console.log(data);},
      err => {console.warn(err);}
    )
  }
}
