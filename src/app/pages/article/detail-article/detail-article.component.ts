import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticleHttpService } from 'src/app/services/article/article-http.service';
import { Article } from 'src/app/models/article/article';

@Component({
  selector: 'app-detail-article',
  templateUrl: './detail-article.component.html',
  styleUrls: ['./detail-article.component.scss']
})
export class DetailArticleComponent implements OnInit {
  article:Article;
  id: number;
  verif:boolean;
  constructor(private route: ActivatedRoute, private articleService: ArticleHttpService) { }

  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.id= params.id;
    });

    this.getbyId(this.id);
  }

  getbyId(id:number) {
    this.articleService.getArticlesById(id).subscribe(
      (article:Article)=> {
        this.article=article;
      
        console.log(article.utilisateur.idutilisateur);
        console.log(localStorage.getItem('idUtilisateur'));
        if(JSON.stringify(article.utilisateur.idutilisateur)===(localStorage.getItem('idUtilisateur'))){
          this.verif=true;
        }else {
          this.verif=false;
        }
        console.log(this.verif);},
      err => {console.warn(err);}
    )
  }

}
