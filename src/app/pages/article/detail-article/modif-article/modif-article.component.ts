import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/models/article/article';
import { ActivatedRoute } from '@angular/router';
import { ArticleHttpService } from 'src/app/services/article/article-http.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-modif-article',
  templateUrl: './modif-article.component.html',
  styleUrls: ['./modif-article.component.scss']
})
export class ModifArticleComponent implements OnInit {
  article:Article;
  id: number;
  articleForm:FormGroup
  categories=['Java', 'Docker', 'Angular', 'Spring', 'JavaScript', 'TypeScript', 'MySQL', 'Autre']

  constructor(private fb:FormBuilder,private router: ActivatedRoute, private articleService: ArticleHttpService) {
    this.articleForm = fb.group({
      nom: "",
      categorie:"",
      contenu:"",
      dateParution:Date,

      })
  }

  ngOnInit() {
    this.router.params.subscribe(params=>{
      this.id= params.id;
    });

    this.getbyId(this.id);
  }

  getbyId(id:number) {
    this.articleService.getArticlesById(id).subscribe(
      (article:Article)=> {
        this.article=article;
        localStorage.setItem("datePublication", JSON.stringify(article.dateParution));
        localStorage.setItem("idArticle",JSON.stringify(article.idarticle));

        },
      err => {console.warn(err);}
    )
  }

  modifArticle=()=>{
    console.log(this.articleForm.value);
    let article = new Article;
    article.idarticle = parseInt(localStorage.getItem('idArticle'))
    article.nom = this.articleForm.value.nom;
    article.categorie = this.articleForm.value.categorie;
    article.contenu= this.articleForm.value.contenu;
    article.dateParution = new Date(localStorage.getItem('dateParution'))
    article.utilisateur.idutilisateur = parseInt(localStorage.getItem('idUtilisateur'));
    console.log(article);

      this.articleService.saveArticle(article).subscribe(
      ()=> {},
      err => {console.warn(err);});
    }

  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '15rem',

      maxHeight: 'auto',
      width: '50rem',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',

};
}
