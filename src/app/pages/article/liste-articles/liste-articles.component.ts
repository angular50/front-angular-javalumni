import { Component, OnInit } from '@angular/core';
import { ArticleHttpService } from 'src/app/services/article/article-http.service';
import { Article } from 'src/app/models/article/article';
import { CatGroup } from 'src/app/models/catGroup/cat-group';
import { FormGroup, FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-liste-articles',
  templateUrl: './liste-articles.component.html',
  styleUrls: ['./liste-articles.component.scss']
})
export class ListeArticlesComponent implements OnInit {
  p: number = 1;
  cats = new Array<CatGroup>();
  data:Article[];
  data2:Article[];
  mesArticles : FormGroup;
  idUtilisateurLocal: number;
  constructor(private fb:FormBuilder, private articleService: ArticleHttpService) {
    this.mesArticles = fb.group({      
      })
   }

  ngOnInit() {
    
    this.idUtilisateurLocal = JSON.parse(localStorage.getItem("idUtilisateur"));
    this.articleService.getArticles().subscribe(
      (data:Article[])=> {
        this.data=data;
        console.log(data);},
      err => {console.warn(err);}
    )
    this.getEtat();
    this.articleExistant();
    this.listeMesArticles();
  }

  listeMesArticles() {
    let idDeMonUtilisateur = JSON.parse(localStorage.getItem("idUtilisateur"));
    this.articleService.getArticlesByIdUtilisateur(idDeMonUtilisateur).subscribe(
      (data:Article[])=> {
        this.data=data;
        console.log(data);},
      err => {console.warn(err);}
    )
  }
  
  articleExistant(){
    this.data2=this.data;
    if ((this.data2).length !== 0){
      return true;
    }else{
      return false
    }
}
 

  getEtat() {
    this.articleService.getArticlesByCat().subscribe(
      (cat:string[])=> {
        cat.forEach(
          cat =>
        this.articleService.getArticlesByCatNb(cat).subscribe(
          (count: number) => {
            this.cats.push(new CatGroup(cat, count))
          },
          err => {console.warn(err);}
        )
        )
        console.log(cat);},
      err => {console.warn(err);}
    )
  }

  orderByCat(cat:string) {
    console.log(cat);
    this.articleService.getArticlesByCatList(cat).subscribe(
      (data:Article[])=> {
        this.data=data;
        console.log(data);},
      err => {console.warn(err);}
    )
  }

}
