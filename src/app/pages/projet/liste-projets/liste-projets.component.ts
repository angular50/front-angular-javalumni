import { Component, OnInit } from '@angular/core';
import { ProjetHttpService } from 'src/app/services/projet/projet-http.service';
import { Projet } from 'src/app/models/projet/projet';
import { EtatGroup } from 'src/app/models/etatGroup/etat-group';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';

@Component({
  selector: 'app-liste-projets',
  templateUrl: './liste-projets.component.html',
  styleUrls: ['./liste-projets.component.scss']
})
export class ListeProjetsComponent implements OnInit {
  p: number = 1;
  etats = new Array<EtatGroup>();
  data:Projet[];
  nb:number;
  nomEtat: string;
  entreprise:boolean;
  idDuProjet:number;
  idProjetSoutenu:number;
  constructor(private projetService: ProjetHttpService) {}

  ngOnInit() {

    this.maMethode(this.nomEtat);

    this.entreprise = localStorage.getItem("siteWeb").startsWith("www");
    this.idDuProjet = parseInt(localStorage.getItem("idProjet"));
    this.idProjetSoutenu = parseInt(localStorage.getItem("projetSoutenu"));

    this.projetService.getProjets().subscribe(
      (data:Projet[])=> {
        this.data=data;
        console.log(data);},
      err => {console.warn(err);}
    )

    this.getEtat()

  }

  maMethode(nom:string) {
if(nom === "enCours") {
  this.nomEtat = "En Cours"
  console.log("coucou");
}
  }

  getEtat() {
    this.projetService.getProjetsByEtat().subscribe(
      (etat:string[])=> {
        etat.forEach(
          etat =>
        this.projetService.getProjetsByEtatNb(etat).subscribe(
          (count: number) => {
            this.etats.push(new EtatGroup(etat, count))
          },
          err => {console.warn(err);}
        )
        )
        console.log(etat);},
      err => {console.warn(err);}
    )
  }

  orderByEtat(etat:string) {
    console.log(etat);
    this.projetService.getProjetsByEtatList(etat).subscribe(
      (data:Projet[])=> {
        this.data=data;
        console.log(data);},
      err => {console.warn(err);}
    )
  }

}
