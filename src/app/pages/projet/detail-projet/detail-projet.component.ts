import { Commentaire } from './../../../models/commentaire/commentaire';
import { Git } from './../../../models/git/git';
import { Component, OnInit } from '@angular/core';
import { ProjetHttpService } from 'src/app/services/projet/projet-http.service';
import { Projet } from 'src/app/models/projet/projet';
import { ActivatedRoute } from '@angular/router';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { getLocaleDateFormat } from '@angular/common';

@Component({
  selector: 'app-detail-projet',
  templateUrl: './detail-projet.component.html',
  styleUrls: ['./detail-projet.component.scss']
})
export class DetailProjetComponent implements OnInit {
  formsubmitted= true;
  projet: Projet;
  gits: Git[];
  commentaires: Commentaire[];
  id: number;
  utilisateurs: Utilisateur[];
  utilisateur: Utilisateur;
  Commentaire: Commentaire;
  inputCommentaire: FormGroup;
  idDuProjet: number;
  splitted:string[];
  splitfonc:string[];
  constructor(private fb: FormBuilder,
              private projetService: ProjetHttpService,
              private utilisateurService: UtilisateurHttpService,
              private route: ActivatedRoute) {
                this.inputCommentaire = fb.group({
                  text: ""
               });
   }

   ngOnInit() {

    this.idDuProjet = parseInt(localStorage.getItem('idProjet'));

    this.route.params.subscribe(params => {
      this.id = parseInt(params.id);
      localStorage.setItem("numProjet", JSON.stringify(this.id));
    });

    this.getbyId(this.id);

    this.projetService.getSoutiensProjetsById(this.id).subscribe(
      (utilisateur: Utilisateur[]) => {
        this.utilisateurs = utilisateur;
        console.log(utilisateur); },
      err => {console.warn(err); }
    );

    this.projetService.getAllGits(this.id).subscribe(
      (gits: Git[]) => {
        this.gits = gits;
         },
      (      err: any) => {console.warn(err); }
    );

    this.projetService.getAllCommentaires(this.id).subscribe(
      (commentaires: Commentaire[]) => {
        this.commentaires = commentaires;
         },
      (      err: any) => {console.warn(err); }
    );

  }

  verifOkIdPojetSoutenu() {
    console.log("Je suis numProjet " +localStorage.getItem('numProjet'));
    console.log("Je suis projetsoutenu " +localStorage.getItem('projetSoutenu'));
    if ((localStorage.getItem('numProjet') === localStorage.getItem('projetSoutenu') ||
    localStorage.getItem('numProjet') === localStorage.getItem('idProjet') ||
    localStorage.getItem('promotion') === "ID62" ||
    localStorage.getItem('promotion') === "ID63" ||
    localStorage.getItem('promotion') === "ID55")) {
      console.log("true");
      return true;
    } else {
      console.log("false");
      return false;
    }
  }

  getbyId(id: number) {
    this.projetService.getProjetsById(id).subscribe(
      (projet: Projet) => {
        this.projet = projet;
      let str=this.projet.technologies.replace(' ', '');
      this.splitted = str.split("/", 3);
      let strfonc=this.projet.fonctionnalites;
      this.splitfonc= strfonc.split("/", 3);
      console.log(this.splitted)
    },
      err => {console.warn(err); }
    );
  }

  readLocalStorageValue(email: string): string {
    return localStorage.getItem(email);
}

  soutenirProjet() {
    const utilisateur = new Utilisateur;
    utilisateur.projetSoutenu = this.id;
    utilisateur.email = JSON.parse(localStorage.getItem('email'));
    utilisateur.password = JSON.parse(localStorage.getItem('password'));
    utilisateur.nom = localStorage.getItem('nom');
    utilisateur.prenom = localStorage.getItem('prenom');
    utilisateur.nomEntreprise = localStorage.getItem ('nomEnt')
    utilisateur.siteWeb = localStorage.getItem('siteWeb');
    utilisateur.presentation = localStorage.getItem('pres');
    utilisateur.idutilisateur = JSON.parse(localStorage.getItem('idUtilisateur'));
    utilisateur.numRCS = JSON.parse(localStorage.getItem('rcssoutenir'));
    utilisateur.linkedin = localStorage.getItem('link');
    utilisateur.projet.idprojet = JSON.parse(localStorage.getItem('idProjet'));
    localStorage.setItem("numProjet", JSON.stringify(this.id));
    

    this.utilisateurService.modifProjetSoutenu(utilisateur).subscribe(
      (utilisateur: Utilisateur) => {
        this.utilisateur = utilisateur;
        localStorage.removeItem('projetSoutenu');
        localStorage.setItem("projetSoutenu", JSON.stringify(utilisateur.projetSoutenu))
      },
      err => {console.warn(err); });
  }

  EnvoyerCommentaire() {
    this.formsubmitted = false;
    // tslint:disable-next-line: new-parens
    const commentaire = new Commentaire;
    commentaire.idprojet = this.id;
    commentaire.text = this.inputCommentaire.value.text;
    commentaire.utilisateur.idutilisateur = JSON.parse(localStorage.getItem('idUtilisateur'));

    this.projetService.saveCommentaire(commentaire).subscribe(
      // tslint:disable-next-line: no-shadowed-variable
      (Commentaire: Commentaire)=> {
        this.Commentaire=Commentaire;
      },
      err => {console.warn(err);});

      this.formsubmitted = true
  }

}
