import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OffreHttpService } from 'src/app/services/offre/offre-http.service';
import { Offre } from 'src/app/models/offre/offre';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-modifier-offre',
  templateUrl: './modifier-offre.component.html',
  styleUrls: ['./modifier-offre.component.scss']
})
export class ModifierOffreComponent implements OnInit {
  offre:Offre;
  id: number;
  offreForm : FormGroup;
  contrats=['CDI','CDD','Stage']
  villes=['Lille', 'Lomme', 'Paris', 'Roubaix', 'Tourcoing']

  constructor(private fb:FormBuilder,private route: ActivatedRoute, private offreService: OffreHttpService) { 

    this.offreForm = fb.group({
      intitule:"",
      description:"",
      localisation:"",
      typeContrat:"",
      datePublication:Date,

    })
  }

  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.id= params.id;
    });

    this.getbyId(this.id);
  }

  getbyId(id:number) {
    this.offreService.getOffresById(id).subscribe(
      (offre:Offre)=> {
        this.offre=offre;
        localStorage.setItem("idOffre", JSON.stringify(offre.idoffre));
        

      },
      err => {console.warn(err);}
    )
  }

  modifOffre=()=>{
    console.log(this.offreForm.value);
    let offre= new Offre;
    offre.idoffre = parseInt(localStorage.getItem('idOffre'))
    offre.intitule= this.offreForm.value.intitule;
    offre.description= this.offreForm.value.description;
    offre.localisation= this.offreForm.value.localisation;
    offre.typeContrat = this.offreForm.value.typeContrat;
    offre.utilisateur.idutilisateur = JSON.parse(localStorage.getItem('idUtilisateur'));

      this.offreService.saveOffre(offre).subscribe(
      ()=> {
       // if (localStorage.length !== 0) {
        //this.routeur.navigate(['/offres/liste-offres']);
       // }
      },
      err => {console.warn(err);});
    }

  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '15rem',
      
      maxHeight: 'auto',
      width: '50rem',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',
    
};

}
  

