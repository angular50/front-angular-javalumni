import { Component, OnInit } from '@angular/core';
import { Offre } from 'src/app/models/offre/offre';
import { ActivatedRoute } from '@angular/router';
import { OffreHttpService } from 'src/app/services/offre/offre-http.service';

@Component({
  selector: 'app-detail-offre',
  templateUrl: './detail-offre.component.html',
  styleUrls: ['./detail-offre.component.scss']
})
export class DetailOffreComponent implements OnInit {
  offre:Offre;
  id: number;
  verif:boolean;
  constructor(private route: ActivatedRoute, private offreService: OffreHttpService) { }

  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.id= params.id;
    });

    this.getbyId(this.id);
    
  }

  getbyId(id:number) {
    this.offreService.getOffresById(id).subscribe(
      (offre:Offre)=> {
        this.offre=offre;
        console.log(offre.utilisateur.idutilisateur);
        console.log(localStorage.getItem('idUtilisateur'));
        if(JSON.stringify(offre.utilisateur.idutilisateur)===(localStorage.getItem('idUtilisateur'))){
          this.verif=true;
        }else {
          this.verif=false;
        }
        console.log(this.verif);

      },
      err => {console.warn(err);} 
    )
  }

  
}
