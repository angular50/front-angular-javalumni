import { Component, OnInit } from '@angular/core';
import { Offre } from 'src/app/models/offre/offre';
import { ActivatedRoute } from '@angular/router';
import { OffreHttpService } from 'src/app/services/offre/offre-http.service';
import { TypeGroup } from 'src/app/models/typeGroup/type-group';

@Component({
  selector: 'app-mes-offres',
  templateUrl: './mes-offres.component.html',
  styleUrls: ['./mes-offres.component.scss']
})
export class MesOffresComponent implements OnInit {
  types = new Array<TypeGroup>();
  id: number;
  data:Offre[];
  pasOffres: boolean;

  constructor(private route: ActivatedRoute, private offreService: OffreHttpService) { }

  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.id= params.id;
  })
  this.listeMesOffres();
  this.getEtat();
  this.pasOffres = JSON.parse(localStorage.getItem("offre")) !== 0;
}
listeMesOffres() {
  this.offreService.getOffresByIdUtilisateur(this.id).subscribe(
    (data:Offre[])=> {
      this.data=data;
      localStorage.setItem("offre", JSON.stringify(data.length));
      console.log(data);},
    err => {console.warn(err);}
  )
}
getEtat() {
  this.offreService.getOffresByTypeContrat().subscribe(
    (type:string[])=> {
      type.forEach(
        type =>
      this.offreService.getOffresByTypeContratNb(type).subscribe(
        (count: number) => {
          this.types.push(new TypeGroup(type, count))
        },
        err => {console.warn(err);}
      )
      )
      console.log(type);},
    err => {console.warn(err);}
  )
}
orderByType(type:string) {
  console.log(type);
  this.offreService.getOffresByTypeContratList(type).subscribe(
    (data:Offre[])=> {
      this.data=data;
      console.log(data);},
    err => {console.warn(err);}
  )
}
}
