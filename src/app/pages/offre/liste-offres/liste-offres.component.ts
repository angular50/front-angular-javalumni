import { Component, OnInit } from '@angular/core';
import { TypeGroup } from 'src/app/models/typeGroup/type-group';
import { Offre } from 'src/app/models/offre/offre';
import { OffreHttpService } from 'src/app/services/offre/offre-http.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-liste-offres',
  templateUrl: './liste-offres.component.html',
  styleUrls: ['./liste-offres.component.scss']
})
export class ListeOffresComponent implements OnInit {
  p: number = 1;
  types = new Array<TypeGroup>();
  id:number;
  data:Offre[];
  data2:Offre[];
  idUtilisateurLocal: number;
  mesOffres : FormGroup;
  constructor(private fb:FormBuilder, private offreService: OffreHttpService) { 
    this.mesOffres = fb.group({      
    })
  }

  ngOnInit() {
    this.idUtilisateurLocal = JSON.parse(localStorage.getItem("idUtilisateur"));
    this.offreService.getOffres().subscribe(
      (data:Offre[])=> {
        this.data=data;
        console.log(data);},
      err => {console.warn(err);}
    )

    this.getEtat();
    this.offreExistant();


  }
  listeMesOffres() {
    let idDeMonUtilisateur = JSON.parse(localStorage.getItem("idUtilisateur"));
    this.offreService.getOffresByIdUtilisateur(idDeMonUtilisateur).subscribe(
      (data:Offre[])=> {
        this.data=data;
        localStorage.setItem("offre", JSON.stringify(data.length));
        console.log(data);},
      err => {console.warn(err);}
    )
  }
  offreExistant(){
    this.data2=this.data;
    if (this.data2.length !== 0){
      return true;
    }else{
      return false
    }
  }


  getEtat() {
    this.offreService.getOffresByTypeContrat().subscribe(
      (type:string[])=> {
        type.forEach(
          type =>
        this.offreService.getOffresByTypeContratNb(type).subscribe(
          (count: number) => {
            this.types.push(new TypeGroup(type, count))
          },
          err => {console.warn(err);}
        )
        )
        console.log(type);},
      err => {console.warn(err);}
    )
  }
  orderByType(type:string) {
    console.log(type);
    this.offreService.getOffresByTypeContratList(type).subscribe(
      (data:Offre[])=> {
        this.data=data;
        console.log(data);},
      err => {console.warn(err);}
    )
  }
}