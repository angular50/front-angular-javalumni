import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EvenementHttpService } from 'src/app/services/evenement/evenement-http.service';
import { Evenement } from 'src/app/models/evenement/evenement';
import { Router } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-creation-evenement',
  templateUrl: './creation-evenement.component.html',
  styleUrls: ['./creation-evenement.component.scss']
})
export class CreationEvenementComponent implements OnInit {
  eventForm: FormGroup;
  villes=['Lille', 'Lomme', 'Paris', 'Roubaix', 'Tourcoing']

  constructor(private fb: FormBuilder,
    private evenenementService : EvenementHttpService,
    private router:Router) {
      this.eventForm = fb.group({
        nom:"",
        description:"",
        localisation:"",
        dateEvenement:Date,
        datePublication: Date,

      })

      
   }
   editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '15rem',
      
      maxHeight: 'auto',
      width: '50rem',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',
    
};
  ngOnInit() {
  }



  envoyerEvenement=()=>{
    console.log(this.eventForm.value);
    let evenement = new Evenement;
    evenement.nom = this.eventForm.value.nom;
    evenement.description = this.eventForm.value.description;
    evenement.localisation= this.eventForm.value.localisation;
    evenement.dateEvenement = this.eventForm.value.dateEvenement;
    evenement.utilisateur.idutilisateur = JSON.parse(localStorage.getItem('idUtilisateur'));

      this.evenenementService.saveEvenement(evenement).subscribe(
      ()=> {
        if (localStorage.length !== 0) {
        this.router.navigate(['/evenements/liste-evenements']);
        }
      },
      err => {console.warn(err);});
  }

}
