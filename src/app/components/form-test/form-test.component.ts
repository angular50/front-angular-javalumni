import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import {utilisateurEmail} from 'src/app/models/utilisateurEmail/utilisateurEmail';


@Component({
  selector: 'app-form-test',
  templateUrl: './form-test.component.html',
  styleUrls: ['./form-test.component.scss']
})
export class FormTestComponent implements OnInit {
  dynamicForm: FormGroup;
  submitted = false;
  arrayBeneficiaire:[];
  email
  constructor(private formBuilder: FormBuilder, private utilisateurService:UtilisateurHttpService) {}
   

  ngOnInit() {
    this.dynamicForm = this.formBuilder.group({
      titre: "",
      description:"",
      technologies:"",
      fonctionnalites:"",
      email:[''],
      numberOfTickets: ['', Validators.required],
        tickets: new FormArray([]),
        
    });
}

// convenience getters for easy access to form fields
get f() { return this.dynamicForm.controls; }
get t() { return this.f.tickets as FormArray; }

onChangeTickets(e) {
    const numberOfTickets = e.target.value || 0;
    if (this.t.length < numberOfTickets) {
        for (let i = this.t.length; i < numberOfTickets; i++) {
            this.t.push(this.formBuilder.group({
              
            }));
        }
    } else {
        for (let i = this.t.length; i >= numberOfTickets; i--) {
            this.t.removeAt(i);
        }
    }
}

getEmail() {
  return [this.utilisateurService.getEmail];
}
onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.dynamicForm.invalid) {
        return;
    }

    // display form values on success
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.dynamicForm.value, null, 4));
}

onReset() {
    // reset whole form back to initial state
    this.submitted = false;
    this.dynamicForm.reset();
    this.t.clear();
}

onClear() {
    // clear errors and reset ticket fields
    this.submitted = false;
    this.t.reset();
}


}