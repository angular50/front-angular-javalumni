import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ArticleHttpService } from 'src/app/services/article/article-http.service';
import { Article } from 'src/app/models/article/article';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Router } from '@angular/router';

@Component({
  selector: 'app-creation-article',
  templateUrl: './creation-article.component.html',
  styleUrls: ['./creation-article.component.scss']
})
export class CreationArticleComponent implements OnInit {
  articleForm : FormGroup;
  categories=['Java', 'Docker', 'Angular', 'Spring', 'JavaScript', 'TypeScript', 'MySQL', 'Autre']

  constructor(private fb:FormBuilder,
    private articleService : ArticleHttpService,
    private router:Router) {
    this.articleForm = fb.group({
      nom: "",
      categorie:"",
      contenu:"",
      dateParution:Date,

      })
    }

    editorConfig: AngularEditorConfig = {
      editable: true,
        spellcheck: true,
        height: '15rem',

        maxHeight: 'auto',
        width: '50rem',
        minWidth: '0',
        translate: 'yes',
        enableToolbar: true,
        showToolbar: true,
        placeholder: 'Enter text here...',
        defaultParagraphSeparator: '',
        defaultFontName: '',
        defaultFontSize: '',
        fonts: [
          {class: 'arial', name: 'Arial'},
          {class: 'times-new-roman', name: 'Times New Roman'},
          {class: 'calibri', name: 'Calibri'},
          {class: 'comic-sans-ms', name: 'Comic Sans MS'}
        ],
        customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText'
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      uploadUrl: 'v1/image',
      sanitize: true,
      toolbarPosition: 'top',

  };
  ngOnInit() {
  }

  envoyerArticle=()=>{
    console.log(this.articleForm.value);
    let article = new Article;
    article.nom = this.articleForm.value.nom;
    article.categorie = this.articleForm.value.categorie;
    article.contenu= this.articleForm.value.contenu;
    article.utilisateur.idutilisateur = parseInt(localStorage.getItem('idUtilisateur'));
    console.log(article);

      this.articleService.saveArticle(article).subscribe(
      ()=> {if (localStorage.length !== 0) {
        this.router.navigate(['/articles/liste-articles']);
        }},
      err => {console.warn(err);});
    }
  }


