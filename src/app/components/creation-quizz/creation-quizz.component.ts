import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Quiz } from 'src/app/models/quiz/quiz';
import { QuizHttpService } from 'src/app/services/quiz/quiz-http.service';
import { Question } from 'src/app/models/question/question';
import { Reponse } from 'src/app/models/reponse/reponse';

@Component({
  selector: 'app-creation-quizz',
  templateUrl: './creation-quizz.component.html',
  styleUrls: ['./creation-quizz.component.scss']
})
export class CreationQuizzComponent implements OnInit {
quizzForm:FormGroup
tabCategories =['Java','MySql']
tabDifficultes = ['Débutant','Intermédiaire','Expert']
dateDuJour = new Date;
idNouveauQuizz:number;

  constructor(private fb:FormBuilder, private quizzService:QuizHttpService) {
    this.quizzForm = fb.group({
nom:"",
categories:"",
difficulte:"",
description:"",
reponse1:"",
reponse2:"",
reponse3:"",
reponse4:"",
reponse5:"",
reponse6:"",
reponse7:"",
reponse8:"",
reponse9:"",
reponse10:"",
reponse11:"",
reponse12:"",
reponse13:"",
reponse14:"",
reponse15:"",
reponse16:"",
reponse17:"",
reponse18:"",
titreQuestion:"",
titreQuestion2:"",
titreQuestion3:"",
titreQuestion4:"",
titreQuestion5:"",
titreQuestion6:""



    })
   }

  ngOnInit() {
  }

  envoyerQuizz=()=>{
    console.log(this.quizzForm.value);
    console.log(this.dateDuJour);
    let quizz= new Quiz;
    quizz.nom=this.quizzForm.value.nom;
    quizz.categorie=this.quizzForm.value.categories;
    quizz.descriptif=this.quizzForm.value.description;
    quizz.difficulte = this.quizzForm.value.difficulte;
    quizz.idutilisateur = JSON.parse(localStorage.getItem('idUtilisateur'));
    quizz.dateParution = this.dateDuJour;
    quizz.utilisateur.idutilisateur = JSON.parse(localStorage.getItem('idUtilisateur'));

     this.quizzService.saveQuizz(quizz).subscribe(
      ()=> {
        this.quizzService.getLastQuizz().subscribe(
          (quiz:Quiz)=> {
            //Recuperer ici l'id du dernier projet sauvé pour l'utiliser dans le localStorage
            quiz = quiz;
           
            localStorage.setItem('idLastQuiz', JSON.stringify((quiz.idquiz)));
            console.log(localStorage.getItem('idLastQuiz'))
     
       
       
       let question = new Question;
       question.quiz.idquiz = parseInt(localStorage.getItem('idLastQuiz'));
       question.categorie =this.quizzForm.value.categories;
       question.difficulte = this.quizzForm.value.difficulte;
       question.corrige= "";
       question.question = this.quizzForm.value.titreQuestion;
       console.log(question);
       this.quizzService.saveQuestion(question).subscribe(
        ()=> {

          this.quizzService.getLastQuestion().subscribe(
            (question:Question)=> {
              //Recuperer ici l'id du dernier projet sauvé pour l'utiliser dans le localStorage
              question = question;
             
              localStorage.setItem('idLastQuestion', JSON.stringify((question.idquestion)));
              console.log(localStorage.getItem('idLastQuestion'))

          let reponse1 = new Reponse;
          let reponse2 = new Reponse;
          let reponse3 = new Reponse;
          reponse1.bonneReponse=1;
          reponse1.reponse = this.quizzForm.value.reponse1;
          reponse1.question.idquestion=parseInt(localStorage.getItem('idLastQuestion'));
          reponse2.bonneReponse=(-1);
          reponse2.reponse = this.quizzForm.value.reponse2;
          reponse2.question.idquestion=parseInt(localStorage.getItem('idLastQuestion'));
          reponse3.bonneReponse=0;
          reponse3.reponse = this.quizzForm.value.reponse3;
          reponse3.question.idquestion=parseInt(localStorage.getItem('idLastQuestion'));
          this.quizzService.saveReponse(reponse1).subscribe(
            ()=> {
              this.quizzService.saveReponse(reponse2).subscribe(
                ()=> {
                  this.quizzService.saveReponse(reponse3).subscribe(
                    ()=> { 
                      let question2 = new Question;
                      question2.quiz.idquiz = parseInt(localStorage.getItem('idLastQuiz'));
                      question2.categorie = this.quizzForm.value.categories;
                      question2.difficulte = this.quizzForm.value.difficulte;
                      question2.corrige= "";
                      question2.question = this.quizzForm.value.titreQuestion2;
                      console.log(question);
                      this.quizzService.saveQuestion(question2).subscribe(
                       ()=> {

                        this.quizzService.getLastQuestion().subscribe(
                          (question:Question)=> {
                            //Recuperer ici l'id du dernier projet sauvé pour l'utiliser dans le localStorage
                            question = question;
                          
                            localStorage.setItem('idLastQuestion2', JSON.stringify((question.idquestion)));
                            console.log(localStorage.getItem('idLastQuestion2'))
            
                        let reponse4 = new Reponse;
                        let reponse5 = new Reponse;
                        let reponse6 = new Reponse;
                        reponse4.bonneReponse=1;
                        reponse4.reponse = this.quizzForm.value.reponse4;
                        reponse4.question.idquestion=parseInt(localStorage.getItem('idLastQuestion2'));
                        reponse5.bonneReponse=(-1);
                        reponse5.reponse = this.quizzForm.value.reponse5;
                        reponse5.question.idquestion=parseInt(localStorage.getItem('idLastQuestion2'));
                        reponse6.bonneReponse=0;
                        reponse6.reponse = this.quizzForm.value.reponse6;
                        reponse6.question.idquestion=parseInt(localStorage.getItem('idLastQuestion2'));
                        this.quizzService.saveReponse(reponse4).subscribe(
                          ()=> {
                            this.quizzService.saveReponse(reponse5).subscribe(
                              ()=> {
                                this.quizzService.saveReponse(reponse6).subscribe(
                                  ()=> { 
                                  let question3 = new Question;
                      question3.quiz.idquiz = parseInt(localStorage.getItem('idLastQuiz'));
                      question3.categorie = this.quizzForm.value.categories;
                      question3.difficulte = this.quizzForm.value.difficulte;
                      question3.corrige= "";
                      question3.question = this.quizzForm.value.titreQuestion3;
                      console.log(question);
                      this.quizzService.saveQuestion(question3).subscribe(
                       ()=> {

                      this.quizzService.getLastQuestion().subscribe(
                        (question:Question)=> {
                          //Recuperer ici l'id du dernier projet sauvé pour l'utiliser dans le localStorage
                          question = question;
                         
                          localStorage.setItem('idLastQuestion3', JSON.stringify((question.idquestion)));
                          console.log(localStorage.getItem('idLastQuestion3'))
            
                      let reponse7 = new Reponse;
                      let reponse8 = new Reponse;
                      let reponse9 = new Reponse;
                      reponse7.bonneReponse=1;
                      reponse7.reponse = this.quizzForm.value.reponse7;
                      reponse7.question.idquestion=parseInt(localStorage.getItem('idLastQuestion3'));
                      reponse8.bonneReponse=(-1);
                      reponse8.reponse = this.quizzForm.value.reponse8;
                      reponse8.question.idquestion=parseInt(localStorage.getItem('idLastQuestion3'));
                      reponse9.bonneReponse=0;
                      reponse9.reponse = this.quizzForm.value.reponse9;
                      reponse9.question.idquestion=parseInt(localStorage.getItem('idLastQuestion3'));
                      this.quizzService.saveReponse(reponse7).subscribe(
                        ()=> {
                          this.quizzService.saveReponse(reponse8).subscribe(
                            ()=> {
                              this.quizzService.saveReponse(reponse9).subscribe(
                                ()=> { 

                                  let question4 = new Question;
                      question4.quiz.idquiz = parseInt(localStorage.getItem('idLastQuiz'));
                      question4.categorie = this.quizzForm.value.categories;
                      question4.difficulte = this.quizzForm.value.difficulte;
                      question4.corrige= "";
                      question4.question = this.quizzForm.value.titreQuestion4;
                      console.log(question4);
                      this.quizzService.saveQuestion(question4).subscribe(
                       ()=> {

                      this.quizzService.getLastQuestion().subscribe(
                        (question:Question)=> {
                          //Recuperer ici l'id du dernier projet sauvé pour l'utiliser dans le localStorage
                          question = question;
                         
                          localStorage.setItem('idLastQuestion4', JSON.stringify((question.idquestion)));
                          console.log(localStorage.getItem('idLastQuestion4'))
            
                      let reponse10 = new Reponse;
                      let reponse11 = new Reponse;
                      let reponse12 = new Reponse;
                      reponse10.bonneReponse=1;
                      reponse10.reponse = this.quizzForm.value.reponse10;
                      reponse10.question.idquestion=parseInt(localStorage.getItem('idLastQuestion4'));
                      reponse11.bonneReponse=(-1);
                      reponse11.reponse = this.quizzForm.value.reponse11;
                      reponse11.question.idquestion=parseInt(localStorage.getItem('idLastQuestion4'));
                      reponse12.bonneReponse=0;
                      reponse12.reponse = this.quizzForm.value.reponse12;
                      reponse12.question.idquestion=parseInt(localStorage.getItem('idLastQuestion4'));
                      this.quizzService.saveReponse(reponse10).subscribe(
                        ()=> {
                          this.quizzService.saveReponse(reponse11).subscribe(
                            ()=> {
                              this.quizzService.saveReponse(reponse12).subscribe(
                                ()=> { 
                                  let question5 = new Question;
                      question5.quiz.idquiz = parseInt(localStorage.getItem('idLastQuiz'));
                      question5.categorie = this.quizzForm.value.categories;
                      question5.difficulte = this.quizzForm.value.difficulte;
                      question5.corrige= "";
                      question5.question = this.quizzForm.value.titreQuestion5;
                      console.log(question5);
                      this.quizzService.saveQuestion(question5).subscribe(
                       ()=> {

                      this.quizzService.getLastQuestion().subscribe(
                        (question:Question)=> {
                          //Recuperer ici l'id du dernier projet sauvé pour l'utiliser dans le localStorage
                          question = question;
                         
                          localStorage.setItem('idLastQuestion5', JSON.stringify((question.idquestion)));
                          console.log(localStorage.getItem('idLastQuestion5'))
            
                      let reponse13 = new Reponse;
                      let reponse14 = new Reponse;
                      let reponse15 = new Reponse;
                      reponse13.bonneReponse=1;
                      reponse13.reponse = this.quizzForm.value.reponse13;
                      reponse13.question.idquestion=parseInt(localStorage.getItem('idLastQuestion5'));
                      reponse14.bonneReponse=(-1);
                      reponse14.reponse = this.quizzForm.value.reponse14;
                      reponse14.question.idquestion=parseInt(localStorage.getItem('idLastQuestion5'));
                      reponse15.bonneReponse=0;
                      reponse15.reponse = this.quizzForm.value.reponse14;
                      reponse15.question.idquestion=parseInt(localStorage.getItem('idLastQuestion5'));
                      this.quizzService.saveReponse(reponse13).subscribe(
                        ()=> {
                          this.quizzService.saveReponse(reponse14).subscribe(
                            ()=> {
                              this.quizzService.saveReponse(reponse15).subscribe(
                                ()=> { let question6 = new Question;
                                  question6.quiz.idquiz = parseInt(localStorage.getItem('idLastQuiz'));
                                  question6.categorie = this.quizzForm.value.categories;
                                  question6.difficulte = this.quizzForm.value.difficulte;
                                  question6.corrige= "";
                                  question6.question = this.quizzForm.value.titreQuestion6;
                                  console.log(question6);
                                  this.quizzService.saveQuestion(question6).subscribe(
                                   ()=> {
            
                                  this.quizzService.getLastQuestion().subscribe(
                                    (question:Question)=> {
                                      //Recuperer ici l'id du dernier projet sauvé pour l'utiliser dans le localStorage
                                      question = question;
                                     
                                      localStorage.setItem('idLastQuestion6', JSON.stringify((question.idquestion)));
                                      console.log(localStorage.getItem('idLastQuestion6'))
                        
                                  let reponse16 = new Reponse;
                                  let reponse17 = new Reponse;
                                  let reponse18 = new Reponse;
                                  reponse16.bonneReponse=1;
                                  reponse16.reponse = this.quizzForm.value.reponse16;
                                  reponse16.question.idquestion=parseInt(localStorage.getItem('idLastQuestion6'));
                                  reponse17.bonneReponse=(-1);
                                  reponse17.reponse = this.quizzForm.value.reponse17;
                                  reponse17.question.idquestion=parseInt(localStorage.getItem('idLastQuestion6'));
                                  reponse18.bonneReponse=0;
                                  reponse18.reponse = this.quizzForm.value.reponse18;
                                  reponse18.question.idquestion=parseInt(localStorage.getItem('idLastQuestion6'));
                                  this.quizzService.saveReponse(reponse16).subscribe(
                                    ()=> {
                                      this.quizzService.saveReponse(reponse17).subscribe(
                                        ()=> {
                                          this.quizzService.saveReponse(reponse18).subscribe(
                                            ()=> { 
            
            
            
            
            
            
                                            },
                                            err => {console.warn(err);}
                                            )
                                            },
                                            err => {console.warn(err);}
                                            )
                                            },
                                            err => {console.warn(err);}
                                            )
                                            },
                                            err => {console.warn(err);}
                                            )
                                            },
                                            err => {console.warn(err);}
                                            )






                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )






                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )
                                  
                                  


                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )
                              },
                              err => {console.warn(err);}
                              )
                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )
                                },
                                err => {console.warn(err);}
                                )
                    },
                    err => {console.warn(err);}
                    )
    
                },
                err => {console.warn(err);}
                )


            },
            err => {console.warn(err);}
            )
          


        },
        err => {console.warn(err);}
       )
       
      },
          err => {console.warn(err);}
          );

        },
        err => {console.warn(err);}
        );
        },
        err => {console.warn(err);}
        );


  
    }
    editorConfig: AngularEditorConfig = {
      editable: true,
        spellcheck: true,
        height: '15rem',
        
        maxHeight: 'auto',
        width: '60rem',
        minWidth: '0',
        translate: 'yes',
        enableToolbar: true,
        showToolbar: true,
        placeholder: 'Enter text here...',
        defaultParagraphSeparator: '',
        defaultFontName: '',
        defaultFontSize: '',
        fonts: [
          {class: 'arial', name: 'Arial'},
          {class: 'times-new-roman', name: 'Times New Roman'},
          {class: 'calibri', name: 'Calibri'},
          {class: 'comic-sans-ms', name: 'Comic Sans MS'}
        ],
        customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText'
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      uploadUrl: 'v1/image',
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [
        [
          'undo',
          'redo',
          'strikeThrough',
          'subscript',
          'superscript',
          'justifyLeft',
          'justifyCenter',
          'justifyRight',
          'justifyFull',
          'indent',
          'outdent',
          'insertUnorderedList',
          'insertOrderedList',
          'heading',
          'fontName'
        ],
        [
        'fontSize',
        'textColor',
        'backgroundColor',
        'customClasses',
        'link',
        'unlink',
        'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode'
      ]

    ]
      
  };
  
  }
