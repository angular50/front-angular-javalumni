import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { Utilisateur } from './../../models/utilisateur/utilisateur';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ProjetHttpService } from '../../services/projet/projet-http.service';
import { Projet } from '../../models/projet/projet';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Git } from 'src/app/models/git/git';

@Component({
  selector: 'app-creation-projet',
  templateUrl: './creation-projet.component.html',
  styleUrls: ['./creation-projet.component.scss']
})
export class CreationProjetComponent implements OnInit {
projetForm: FormGroup;
utilisateur: Utilisateur;
idprojetOK: number;
idOK: number;
techno: string;
fonctions: string;
technosbacks=['Java','PHP','Python','C#'];
technosfronts=['Angular','IONIC','JEE','REACT'];
technosbases=['MySQL','PostGreSQL','MongoDB'];
utilProjet:Utilisateur

constructor(private fb:FormBuilder,
            private projetService: ProjetHttpService,
            private utilisateurService: UtilisateurHttpService) {
this.projetForm = fb.group({
titre: "",
description:"",
fonctionnalites1:"",
fonctionnalites2:"",
fonctionnalites3:"",
groupe:"",
technosbacks:"",
technosfronts:"",
technosbases:"",
gits1:"",
gits2:""


});

     }

  ngOnInit() {
    this.utilisateurService.getUtilisateurById(parseInt(localStorage.getItem('idUtilisateur'))).subscribe(
      (utilisateur: Utilisateur) => {
        this.utilProjet = utilisateur;
        console.log(utilisateur); },
      err => {console.warn(err); }
    );
    
  }

  envoyer=()=>{
    console.log(this.projetForm.value);
    //this.technologies =  new technologies;
    this.techno = (this.projetForm.value.technosbacks + "/" + this.projetForm.value.technosfronts + "/" + this.projetForm.value.technosbases);
    this.fonctions = (this.projetForm.value.fonctionnalites1 + "/" + this.projetForm.value.fonctionnalites2 + "/" + this.projetForm.value.fonctionnalites3);
    let projet = new Projet;
    projet.idprojet = JSON.parse(localStorage.getItem("idUtilisateur"));
    projet.titre = this.projetForm.value.titre;
    projet.description = this.projetForm.value.description;
    projet.technologies = this.techno;
    projet.fonctionnalites = this.fonctions;
    projet.groupe = this.projetForm.value.groupe;

    this.projetService.saveProjet(projet).subscribe(
      ()=> {
        localStorage.setItem("test", JSON.stringify(projet.idprojet));
        this.idOK = JSON.parse(localStorage.getItem('test'));
        
        this.projetService.getLastProjet().subscribe(
          (projet:Projet)=> {
            //Recuperer ici l'id du dernier projet sauvé pour l'utiliser dans le localStorage
            projet = projet;
            localStorage.removeItem('idProjet');
            localStorage.setItem('idProjet2', JSON.stringify((projet.idprojet) + 2));
            this.idprojetOK = JSON.parse(localStorage.getItem('idProjet2'));

        

            

            let utilisateur = new Utilisateur;
            utilisateur.nom = this.utilProjet.nom;
            utilisateur.prenom = this.utilProjet.prenom;
            utilisateur.idutilisateur = JSON.parse(localStorage.getItem('idUtilisateur'));
            utilisateur.numPE = JSON.parse(localStorage.getItem('numPE'));
            utilisateur.promotion = localStorage.getItem('promotion');
            utilisateur.email = JSON.parse(localStorage.getItem('email'));
            utilisateur.password = this.utilProjet.password;
            utilisateur.formation = this.utilProjet.formation;
            utilisateur.dateDebut = this.utilProjet.dateDebut;
            utilisateur.dateFin = this.utilProjet.dateFin;
            utilisateur.projet.idprojet = projet.idprojet;
            utilisateur.presentation = this.utilProjet.presentation;
            utilisateur.linkedin = this.utilProjet.linkedin;
            console.log(utilisateur);
            

            this.utilisateurService.modifUtilisateur(utilisateur).subscribe(
          () => { let git = new Git;
            git.idprojet = projet.idprojet;
            git.url = this.projetForm.value.gits1;
            git.projet.idprojet = projet.idprojet;
            console.log(git)
            
            this.projetService.saveGit(git).subscribe(
              ()=> {

                let git = new Git;
                git.idprojet = projet.idprojet;
                git.url = this.projetForm.value.gits2;
                git.projet.idprojet = projet.idprojet;
                console.log(git);
                this.projetService.saveGit(git).subscribe(
                  ()=> { },

                  err => {console.warn(err);}
              )
            
          },
          err => {console.warn(err);});

          },
        err => {console.warn(err);});
       
      },
          err => {console.warn(err);});

   

         
                
                
                
                err => {console.warn(err);}
                  })
        }
        editorConfig: AngularEditorConfig = {
          editable: true,
            spellcheck: true,
            height: '15rem',
            
            maxHeight: 'auto',
            width: '50rem',
            minWidth: '0',
            translate: 'yes',
            enableToolbar: true,
            showToolbar: true,
            placeholder: 'Enter text here...',
            defaultParagraphSeparator: '',
            defaultFontName: '',
            defaultFontSize: '',
            fonts: [
              {class: 'arial', name: 'Arial'},
              {class: 'times-new-roman', name: 'Times New Roman'},
              {class: 'calibri', name: 'Calibri'},
              {class: 'comic-sans-ms', name: 'Comic Sans MS'}
            ],
            customClasses: [
            {
              name: 'quote',
              class: 'quote',
            },
            {
              name: 'redText',
              class: 'redText'
            },
            {
              name: 'titleText',
              class: 'titleText',
              tag: 'h1',
            },
          ],
          uploadUrl: 'v1/image',
          sanitize: true,
          toolbarPosition: 'top',
          
      };
    }



